var elixir = require('laravel-elixir');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less(['bootstrap.less', 'materialadmin.less', 'libs/bootstrap-datepicker/datepicker3.less']);

    mix.copy('resources/assets/css', 'public/css');
    //mix.copy('resources/assets/js/libs/jquery-ui/themes/smoothness/jquery-ui.min.css', 'public/backend/css/jquery-ui.min.css');
    //mix.copy('resources/assets/css/toolslab.jquery-ui.css', 'public/backend/css/toolslab.jquery-ui.css')
    mix.copy('resources/assets/js/libs/select2/dist/css/select2.min.css', 'public/backend/css/select2.min.css');
    mix.copy('resources/assets/js/libs/multiselect/css/multi-select.css','public/backend/css/multi-select.css');
    mix.copy('resources/assets/css/toastr.css','public/backend/css/toastr.css');
    mix.copy('resources/assets/js/libs/bootstrap-tagsinput/dist/bootstrap-tagsinput.css','public/backend/css/bootstrap-tagsinput.css');
    mix.copy('resources/assets/js/libs/bootstrap-colorpicker/css/colorpicker.css','public/backend/css/colorpicker.css');

    mix.copy('resources/assets/js/libs/respond/dest/respond.min.js', 'public/backend/js/respond.min.js');
    mix.copy('resources/assets/js/libs/html5shiv/dist/html5shiv.min.js', 'public/backend/js/html5shiv.min.js');
    mix.copy('resources/assets/js/libs/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css', 'public/backend/css/bootstrap-slider.min.css');

    mix.scripts([
        'libs/jquery/dist/jquery.min.js',
        'libs/bootstrap/dist/js/bootstrap.min.js',
        'libs/toastr/toastr.min.js',
        'libs/spinjs/spin.js',
        'libs/underscore/underscore-min.js',
        'libs/spinjs/jquery.spin.js',
        'libs/autosize/dist/autosize.min.js',
        'libs/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js',
        //'libs/jquery-ui/jquery-ui.min.js',
        'libs/jquery-validation/dist/jquery.validate.min.js',
        'libs/jquery.cookie/jquery.cookie.js',
        'libs/jszip/dist/jszip.min.js',
        'core/source/App.js',
        'core/source/AppNavigation.js',
        'core/source/AppOffcanvas.js',
        'core/source/AppCard.js',
        'core/source/AppForm.js',
        'core/source/AppNavSearch.js',
        'core/source/AppVendor.js',
        'core/source/slick.js',
        'libs/select2/dist/js/select2.full.min.js',
        'libs/multiselect/js/jquery.multi-select.js',
        'libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'libs/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
        'libs/jquery.inputmask/dist/jquery.inputmask.bundle.js',
        'jquery.truncate.js',
        'libs/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
        'libs/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
    ], 'public/backend/js/all.js');

    mix.stylesIn("public/css");
    mix.version([
        'css/all.css',
        'js/all.js'
    ]);

    mix.copy('resources/assets/css/kendo.css', 'public/backend/css/kendo.css');
    mix.copy('resources/assets/js/kendo/kendo.toolslab-grid.base.js', 'public/backend/js/kendo.toolslab-grid.base.js')
    mix.copy('resources/assets/js/kendo/pako_deflate.min.js', 'public/backend/js/pako_deflate.min.js');
    mix.copy('resources/assets/js/kendo/kendo.all.min.js','public/backend/js/kendo.all.min.js');
    mix.copy('resources/assets/js/kendo/messages/kendo.messages.ru-RU.min.js','public/backend/js/kendo.messages.ru-RU.min.js');
    mix.copy('resources/assets/js/kendo/cultures/kendo.culture.ru-RU.min.js','public/backend/js/kendo.culture.ru-RU.min.js');
    mix.copy('resources/assets/js/script-card.js','public/backend/js/script-card.js');
    mix.copy('resources/assets/js/libs/handlebars/handlebars.min.js', 'public/backend/js/handlebars.min.js');

    mix.copy('resources/assets/js/libs/jquery.inputmask/dist/jquery.inputmask.bundle.min.js','public/backend/demo/js/jquery.inputmask.bundle.min.js');
    mix.copy('resources/assets/js/libs/jquery.cookie/jquery.cookie.js', 'public/backend/demo/js/jquery.cookie.js');
    mix.copy('resources/assets/demo-landing/fonts/','public/backend/demo/fonts/');
    mix.copy('resources/assets/demo-landing/images/','public/backend/demo/images/');


    mix.copy(
        'resources/assets/js/libs/jquery.fancybox/fancybox/jquery.fancybox.pack-1.3.4.js',
        'public/backend/js/jquery.fancybox-1.3.4.pack.js'
    );

    mix.copy(
        'resources/assets/js/libs/jquery.fancybox/fancybox/jquery.fancybox-1.3.4.css',
        'public/backend/css/jquery.fancybox.css'
    );
    mix.copy('resources/assets/css/jquery-ui-theme.css', 'public/backend/css/jquery-ui-theme.css');
    mix.copy('resources/assets/css-kendo/kendo.dataviz.min.css', 'public/backend/css/kendo.dataviz.min.css');
    mix.copy('resources/assets/css-kendo/kendo.default.min.css', 'public/backend/css/kendo.default.min.css');
    mix.copy('resources/assets/css/jquery-ui-filemanager-theme.css', 'public/backend/css/jquery-ui-filemanager-theme.css');
    mix.copy('resources/assets/css-kendo/kendo.dataviz.default.min.css', 'public/backend/css/kendo.dataviz.default.min.css');
    mix.copy('resources/assets/js/jamur.js', 'public/backend/js/jamur.js');

    mix.copy('resources/assets/js/libs/jquery-ui/jquery-ui.min.js', 'public/backend/js/jquery-ui.min.js');
    mix.copy('resources/assets/js/libs/jquery-ui/ui/slider.js', 'public/backend/js/jquery-slider.min.js');
});

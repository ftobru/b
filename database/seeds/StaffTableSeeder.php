<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class StaffTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Staff::class, 5)->create()->each(function($s){
            /** @var \App\Entities\Salon $s */
            $s->services()->save(factory(\App\Entities\Service::class)->make());
        });
    }
}

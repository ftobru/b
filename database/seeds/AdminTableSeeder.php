<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        // 'name', 'password', 'email', 'phone', 'last_active'
        $admin = [
            'name' => 'admin',
            'password' => 'bfab123zxc',
            'email' => 'admin@bfab.ru',
            'phone' => '+7 913 455 0042'
        ];

        /** @var \App\Repositories\Eloquent\AdminRepositoryEloquent $repository */
        $repository = App::make('App\Contracts\Repositories\AdminRepository');

        $repository->create($admin);

    }
}

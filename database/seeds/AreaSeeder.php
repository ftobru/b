<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $repository = App::make('App\Contracts\Repositories\AreaRepository');
        Collection::make([
            [
                'name' => 'Bangsar',
                'lat' => 3.133422,
                'lng' => 101.668674,
            ],
            [
                'name' => 'Bukit Jalil',
                'lat' => 3.057775,
                'lng' => 101.689784,
            ],
            [
                'name' => 'Cheras',
                'lat' => 3.112246,
                'lng' => 101.714542,
            ],
            [
                'name' => 'Damansara Heights',
                'lat' => 3.147942,
                'lng' => 101.668018,
            ],
            [
                'name' => 'Damansara Perdana',
                'lat' => 3.182797,
                'lng' => 101.619437,
            ],
            [
                'name' => 'KLCC',
                'lat' => 3.159022,
                'lng' => 101.713688,
            ],
            [
                'name' => 'Desa Parkcity',
                'lat' => 3.099622,
                'lng' => 101.686775,
            ],
            [
                'name' => 'Kota Damansara',
                'lat' => 3.178444,
                'lng' => 101.585562,
            ],
            [
                'name' => 'Mont Kiara',
                'lat' => 3.170399,
                'lng' => 101.651166,
            ],
            [
                'name' => 'Puchong',
                'lat' => 3.073136,
                'lng' => 101.657993,
            ],
            [
                'name' => 'Sri Hartamas',
                'lat' => 3.162182,
                'lng' => 101.650357,
            ],
            [
                'name' => 'Sri Petaling',
                'lat' => 3.072594,
                'lng' => 101.682526,
            ],
            [
                'name' => 'Taman Danau Desa',
                'lat' => 3.100191,
                'lng' => 101.687485,
            ],
            [
                'name' => 'Taman Duta',
                'lat' => 3.168133,
                'lng' => 101.673013,
            ],
            [
                'name' => 'Wangsa Maju',
                'lat' => 3.203917,
                'lng' => 101.737188,
            ],
        ])->each(function($attributes) use ($repository){
            $repository->create($attributes);            
        });
    }
}

<?php

use Illuminate\Database\Seeder;

class FlushSalonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('TRUNCATE salons CASCADE;');
        DB::statement('TRUNCATE phones CASCADE;');
        DB::statement('TRUNCATE emails CASCADE;');
        DB::statement('TRUNCATE sites CASCADE;');
        DB::statement('TRUNCATE vendors CASCADE;');
    }
}

<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

/**
 * Class VendorTableSeeder
 */
class VendorTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Vendor::class, 100)->create()->each(function($v){
            /** @var \App\Entities\Vendor $v */
            $v->salons()->save(factory(\App\Entities\Salon::class)->make());
        });
        factory(\App\Entities\Vendor::class)->create(['phone' => '89237550040', 'password' => 'test'])->salons()->save(factory(\App\Entities\Salon::class)->make());
    }
}

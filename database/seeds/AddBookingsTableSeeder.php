<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class AddBookingsTableSeeder extends Seeder
{
    public function run()
    {
        /** @var \App\Contracts\Repositories\BookingRepository $booking */
        $booking = App::make('App\Contracts\Repositories\BookingRepository');

        $bookingData = [
            'salon_id' => 754,
            'user_id' => 1,
            'service_id' => 25,
            'start_time' => \Carbon\Carbon::now(),
            'end_time' => \Carbon\Carbon::now()->addHour(),
            'status' => 0,
            'payment_id' => 1,
        ];

        $booking->create($bookingData);
    }
}

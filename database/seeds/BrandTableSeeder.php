<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class BrandTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Brand::class, 50)->create();
    }
}

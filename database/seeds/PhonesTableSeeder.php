<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PhonesTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Phone::class, 10)->create()->each(function($c){
            /** @var \App\Entities\Comment $c */
            $c->salons()->save(factory(\App\Entities\Salon::class)->make())->create();
        });
    }
}

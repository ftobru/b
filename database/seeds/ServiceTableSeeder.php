<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ServiceTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Service::class, 5)->create()->each(function($s){
            /** @var \App\Entities\Salon $s */
            $s->staffs()->save(factory(\App\Entities\Staff::class)->make());
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class SalonRealDataTableSeeder extends Seeder
{
    public $repositories;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('FlushSalonTableSeeder');
        $repository = App::make('App\Contracts\Repositories\SalonRepository');
        $repositoryPhone = App::make('App\Contracts\Repositories\PhoneRepository');
        $repositoryEmail = App::make('App\Contracts\Repositories\EmailRepository');
        $repositorySite = App::make('App\Contracts\Repositories\SiteRepository');
        Collection::make([
            [
                'name' => 'Brazilian ¬ Pro Waxing Salon',
                'address' => 'No. 11 - 1, Jalan Solaris 3, Solaris Mont Kiara, 50480 Kuala Lumpur',
                'lat' => 3.175258,
                'lng' => 101.659834,
                'phone' => ['number' => '03 6204 0762'],
            ],
            [
                'name' => 'Cocomo Salon',
                'address' => 'Ground Floor, 13, Jalan Solaris 3, Solaris Mont Kiara, 50480 Kuala Lumpur',
                'lat' => 3.175428,
                'lng' => 101.659396,
                'phone' => ['number' => '012 281 6948'],
                'site' => ['name' => 'https://www.facebook.com/Salun-Cocomo-113570708723341/timeline/'],
            ],
            [
                'name' => 'Buff N Shine Salon',
                'address' => '13, Ground Floor, Jalan Solaris 3, Solaris Mont Kiara, 50480 Kuala Lumpur',
                'lat' => 3.175403,
                'lng' => 101.659396,
                'phone' => ['number' => '03 6201 0417'],
            ],
            [
                'name' => 'Shamayel ¬ Hair & Beauty Salon',
                'address' => 'Lot 11.1, 1st Floor, Jalan Solaris, Solaris Mont Kiara,50480, Kuala Lumpur',
                'lat' => 3.174288,
                'lng' => 101.660509,
                'phone' => ['number' => '03 6204 0541'],
            ],
            [
                'name' => 'Yippo Hair Style',
                'address' => 'K-01-04 Solaris Mont Kiara, No.2, Jalan Solaris, 50480 Kuala Lumpur',
                'lat' => 3.175879,
                'lng' => 101.659048,
                'phone' => ['number' => '03 6211 7897'],
                'site' => ['name' => 'https://www.facebook.com/yippohairstyle.lee/timeline'],
            ],
            [
                'name' => 'De Tags Beauty & Hair Saloon',
                'address' => 'No 15A, First Floor,Sea Park,Jalan 20/1 - 21/37, 46300 Petaling Jaya, Selangor',
                'lat' => 3.108808,
                'lng' => 101.625413,
                'phone' => ['number' => '03 7865 3433'],
                'email' => ['name' => 'detagshairsalon26@gmail.com'],
                'site' => ['name' => 'https://www.facebook.com/DeTagsBeautyHairSalon'],
            ],
            [
                'name' => 'Dhiya Ammani',
                'address' => '06-03-, Bangunan PJ21, SS3/39, Petaling Jaya, Kelana Jaya, Selangor',
                'lat' => 3.098222,
                'lng' => 101.594895,
                'phone' => ['number' => '014 221 8672'],
                'email' => ['name' => 'admin@dhiyaammani.com'],
                'site' => ['name' => 'http://dhiyaammani.com'],
            ],
            [
                'name' => 'Mirror Mirror Hair Salon & Makeup Studio',
                'address' => '15-1 Jalan Solaris 4. Solaris Mont Kiara., 50480 Kuala Lumpur',
                'lat' => 3.175913,
                'lng' => 101.659640,
                'phone' => ['number' => '010 535 1953'],
                'email' => ['name' => 'enquiry@officialmirrormirror.com'],
                'site' => ['name' => 'http://www.officialmirrormirror.com'],
            ],
            [
                'name' => "Lavina's Hair & Beauty Saloon",
                'address' => "No. 11-1, Jalan Solaris, Solaris Mont' Kiara,50480, Kuala Lumpur",
                'lat' => 3.175890,
                'lng' => 101.658973,
                'phone' => ['number' => '03 6206 2019'],
                'site' => ['name' => 'https://www.facebook.com/lavinashairandbeautysaloon/timeline'],
            ],
            [
                'name' => "Lavina's Hair & Beauty Saloon (1st Branch)",
                'address' => "24 Jalan Mamanda 4, Taman Dato Ahmad Razali, 68000 Ampang, 68000 Selangor",
                'lat' => 3.157988,
                'lng' => 101.752270,
                'phone' => ['number' => '03 4252 0541'],
                'site' => ['name' => 'https://www.facebook.com/lavinashairandbeautysaloon/timeline'],
            ],
            [
                'name' => "White Feather Spa & Beauty Salon",
                'address' => "17 G ,Jalan Solaris 3,Jalan Mont Kiara, 50480 Kuala Lumpur",
                'lat' => 3.175257,
                'lng' => 101.659165,
                'phone' => ['number' => '03 6206 3477'],
            ],
            [
                'name' => "Juicy Colors Nail Studio",
                'address' => "No.13-1, Jalan solaris 3, 50480 Kuala Lumpr",
                'lat' => 3.175333,
                'lng' => 101.659909,
                'phone' => ['number' => '011 3305 6288'],
                'email' => ['name' => 'cambrins@hotmail.com'],
            ],
            [
                'name' => "Susan & Guy Unisex Hair Saloon",
                'address' => "22A Jalan 52/4 , 46200 Petaling Jaya, Selangor",
                'lat' => 3.100442,
                'lng' => 101.645532,
                'phone' => ['number' => '03-7957 8124 / 012 657 0707'],
            ],
            [
                'name' => "Skin Town",
                'address' => "118, Sutera Tanjung 8/3 Taman Sutera Utama, 81300 Skudai Johor",
                'lat' => 1.516088,
                'lng' => 103.668003,
            ],
            [
                'name' => "Mauita Spa",
                'address' => "No.9, 1st Floor, Solaris 3, 50480 Mont Kiara, Selangor",
                'lat' => 3.175333,
                'lng' => 101.659446,
                'phone' => ['number' => '03 6211 0983'],
            ],
            [
                'name' => "S Spa",
                'address' => "3rd Floor, 7-3, Jalan Solaris, Solaris Mont Kiara, 50480 Kuala Lumpur",
                'lat' => 3.175914,
                'lng' => 101.659203,
                'phone' => ['number' => '03 6203 0434'],
            ],
            [
                'name' => "Jari Spa",
                'address' => "A2-U1-5, Solaris Dutamas, No. 1 Jalan Dutamas 1, 50480 Kuala Lumpur",
                'lat' => 3.172536,
                'lng' => 101.666558,
                'phone' => ['number' => '03 6205 3624'],
                'email' => ['name' => 'admin@jarispa.com'],
                'site' => ['name' => 'https://www.facebook.com/Jarispa'],
            ],
             ])->each(function($attributes) use ($repository,$repositoryPhone,$repositoryEmail,$repositorySite){
                 $vendor = factory(\App\Entities\Vendor::class)->create(['password' => '123456']);
                 $attributes['vendor_id'] = $vendor->id;
                 $salon = $repository->create($attributes);
                 if(isset($attributes['email'])){
                    $salon->email()->save($repositoryEmail->create($attributes['email']));
                 }
                 if(isset($attributes['phone'])){
                    $salon->phone()->save($repositoryPhone->create($attributes['phone']));
                 }
                 if(isset($attributes['site'])){
                    $salon->site()->save($repositorySite->create($attributes['site']));
                 }
            });
    }
}

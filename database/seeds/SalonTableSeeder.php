<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SalonTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Salon::class, 5)->create()->each(function($s){
            /** @var \App\Entities\Salon $s */
            $s->comments()->save(factory(\App\Entities\Comment::class)->make());
            $s->brands()->save(factory(\App\Entities\Brand::class)->make());
            $s->pictures()->save(factory(\App\Entities\Picture::class)->make());
        });
    }
}

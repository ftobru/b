<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class CommentTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Entities\Comment::class, 10)->create()->each(function($c){
            /** @var \App\Entities\Comment $c */
        });
    }
}

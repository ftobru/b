<?php

use App\References\UserReference;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

/**
 * Class AddUserByTestTableSeeder
 */
class AddUserByTestTableSeeder extends Seeder
{
    public function run()
    {
        /** @var \App\Repositories\Eloquent\UserRepositoryEloquent $userRepository */
        $userRepository = app('App\Contracts\Repositories\UserRepository');
        $userData = [
            'status' => UserReference::STATUS_ACTIVE,
            'name' => 'Nikita',
            'surname' => 'Volkov',
            'email' => 'litebackend@gmail.com',
            'password' => 'test',
            'phone' => '+79134550042'
        ];

        $userRepository->create($userData);
    }
}

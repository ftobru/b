<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRoutinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routines', function(Blueprint $blueprint){
            $blueprint->time('opening_time')->nullable()->change();
            $blueprint->time('closing_time')->nullable()->change();
            $blueprint->time('break_start')->nullable()->change();
            $blueprint->time('break_finish')->nullable()->change();
            $blueprint->tinyInteger('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routines', function(Blueprint $blueprint){
            $blueprint->dropColumn('status');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIdFromPictures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pictures', function(Blueprint $blueprint){
            $blueprint->dropColumn('id');
        });
        Schema::table('pictures', function(Blueprint $blueprint){
            $blueprint->bigIncrements('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pictures', function(Blueprint $blueprint){
            $blueprint->dropColumn('id');
        });

        Schema::table('pictures', function(Blueprint $blueprint){
            $blueprint->bigInteger('id');
        });
    }
}

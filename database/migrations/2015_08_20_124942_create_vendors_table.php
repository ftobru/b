<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->string('name');
            $table->string('email');
            $table->string('password', 60);
            $table->timestamp('last_active')->nullable();
            $table->string('phone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineSalonPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routine_salon', function(Blueprint $table) {
            $table->integer('routine_id')->unsigned()->index();
            $table->foreign('routine_id')->references('id')->on('routines')->onDelete('cascade');
            $table->integer('salon_id')->unsigned()->index();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routine_salon');
    }
}

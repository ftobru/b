<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('user_id');
            $table->decimal('amount');
            $table->bigInteger('promo_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('response_code')->nullable();
            $table->string('response_text')->nullable();
            $table->jsonb('deep')->nullable();
            $table->string('payment_type');
            $table->bigInteger('service_id');
            $table->bigInteger('salon_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

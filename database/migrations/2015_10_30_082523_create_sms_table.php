<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('to');
            $table->string('message', 4096);
            $table->tinyInteger('type');
            $table->string('status')->nullable();
            $table->bigInteger('recipient_id')->nullable();
            $table->bigInteger('api_id')->nullable();
            $table->string('destination')->nullable();
            $table->string('errorCode')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSalonPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('salon_site', function(Blueprint $table) {
            $table->integer('site_id')->index();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->integer('salon_id')->index();
            $table->foreign('salon_id')->references('id')->on('salons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_site');
    }
}

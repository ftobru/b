<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineStaffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routine_staffs', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->time('work_time_start')->nullable();
            $table->time('work_time_end')->nullable();
            $table->bigInteger('salon_id');
            $table->bigInteger('staff_id');
            $table->enum('weekday', [
                'Mon',
                'Tue',
                'Wed',
                'Thu',
                'Fri',
                'Sat',
                'Sun'
            ]);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routine_staffs');
    }
}

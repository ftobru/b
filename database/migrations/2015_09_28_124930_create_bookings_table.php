<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function(Blueprint $blueprint){
            $blueprint->bigIncrements('id');
            $blueprint->integer('salon_id');
            $blueprint->integer('user_id');
            $blueprint->integer('service_id');
            $blueprint->timestamp('start_time');
            $blueprint->timestamp('end_time');
            $blueprint->tinyInteger('status');
            $blueprint->integer('payment_id');
            $blueprint->boolean('is_discount')->default(false);
            $blueprint->integer('promo_id')->nullable();
            $blueprint->char('descriptions')->nullable();
            $blueprint->timestamps();
            $blueprint->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}

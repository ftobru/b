<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\References\SalonReference;
use App\Support\PathHelper;
use Faker\Generator;
use Illuminate\Support\Collection;

$factory->define(App\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
        'surname' =>  $faker->lastName,
        'last_active' => $faker->dateTime
    ];
});

$factory->define(App\Entities\Brand::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'logo' => PathHelper::cut($faker->image(public_path('images/brands'))),
    ];
});


$factory->define(\App\Entities\Comment::class, function (Faker\Generator $faker){
    return [
        'status' => $faker->numberBetween(0, 1),
        'message' => $faker->text(),
        'user_id' => factory(\App\Entities\User::class)->create()->id,
        'salon_id' => factory(\App\Entities\Salon::class)->create()->id
    ];
});

$factory->define(\App\Entities\Phone::class, function(Faker\Generator $faker) {
    return [
        'number' => $faker->phoneNumber
    ];
});

$factory->define(\App\Entities\Salon::class, function(Faker\Generator $faker){
    return [
        'status' => $faker->numberBetween(0, 1),
        'type' => SalonReference::CruiseShipSpa,
        'name' => $faker->company,
        'address' => $faker->address,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'vendor_id' => factory(App\Entities\Vendor::class)->create()->id,
        'rating' => $faker->numberBetween(1, 1000)
    ];
});

$factory->define(\App\Entities\Vendor::class, function(Faker\Generator $faker){
    return [
        'status' => $faker->numberBetween(0, 1),
        'type' => $faker->numberBetween(0, 1),
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => $faker->password,
        'phone' => $faker->phoneNumber,

    ];
});

$factory->define(\App\Entities\Picture::class, function(\Faker\Generator $faker){
    return [
        'pictures' => Collection::make([
            '580х340' => PathHelper::cut($faker->image(public_path('images/salons'), 580, 340)),
            '255х155' => PathHelper::cut($faker->image(public_path('images/salons'), 255, 155)),
        ])
    ];
});

$factory->define(\App\Entities\Staff::class, function(Generator $faker){
    return [
        'title' => $faker->title,
        'name' => $faker->name,
        'salon_id' => factory(App\Entities\Salon::class)->create()->id
    ];
});

$factory->define(\App\Entities\Service::class, function(Generator $faker){
    return [
        'name' => $faker->name,
        'duration' => $faker->numberBetween(0, 1000),
        'price' => $faker->numberBetween(0, 10000),
        'salon_id' => factory(App\Entities\Salon::class)->create()->id,
    ];
});

<?php

namespace App\Entities;

use App\References\ServiceTypeReference;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Service extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name', 'duration', 'price', 'old_price', 'salon_id'];

    protected $appends = ['type_label'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staffs()
    {
        return $this->belongsToMany(Staff::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function salon()
    {
        return $this->hasOne(Salon::class);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getTypeLabelAttribute($value)
    {
        $refernce = new ServiceTypeReference();

        $serviceTypeId = array_get($this->attributes, 'service_type_id', 1);
        return $refernce->types()[$serviceTypeId];
    }


}

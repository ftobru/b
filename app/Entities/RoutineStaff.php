<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RoutineStaff extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['work_time_start', 'work_time_end', 'weekday', 'salon_id', 'staff_id'];


}

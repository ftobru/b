<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Picture
 * @package App\Entities
 * @property int id
 * @property Collection pictures
 * @property int salon_id
 */
class Picture extends Model
{
    protected $fillable = ['pictures', 'salon_id'];

    protected $table = 'pictures';

    /**
     * @param Collection $value
     */
    public function setPicturesAttribute($value)
    {
        $this->attributes['pictures'] = json_encode($value);
    }

    /**
     * @return Collection
     */
    public function getPicturesAttribute()
    {
        return new Collection(json_decode($this->attributes['pictures'], true));
    }

    public function salon()
    {
        return $this->belongsTo('App\Entities\Salon');
    }
}
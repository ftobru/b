<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Staff
 * @package App\Entities
 */
class Staff extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'staffs';
    /**
     * @var array
     */
    protected $fillable = ['name', 'title', 'salon_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function routineStaffs()
    {
        return $this->hasMany(RoutineStaff::class);
    }

}

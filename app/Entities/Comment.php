<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Comment
 * @package App\Entities
 * @property string message
 * @property int user_id
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Comment extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $fillable = ['message', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salons()
    {
        return $this->hasMany('App\Entities\Salon');
    }

}

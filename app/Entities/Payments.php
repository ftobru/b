<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Payments extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'status',
        'user_id',
        'amount',
        'promo_id',
        'transaction_id',
        'response_code',
        'response_text',
        'deep',
        'service_id',
        'salon_id'
    ];

    /**
     * @param $value
     * @return array|static
     */
    public function getDeepAttribute($value)
    {
        return $this->attributes['deep']
            ? Collection::make(json_decode($this->attributes['deep'])) : $this->attributes['deep'];
    }

    /**
     * @param $value
     */
    public function setDeepAttribute($value)
    {
        $this->attributes['deep'] = (is_array($value) ? json_encode($value) : $value);
    }


}

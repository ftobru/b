<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Phone
 * @package App\Entities
 * @property string number
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Phone extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $fillable = ['number'];


    public function salons()
    {
        return $this->belongsToMany(Salon::class);
    }
    
    /*public function setNumberAttribute($value)
    {
        $this->number = $value;
    }*/
}

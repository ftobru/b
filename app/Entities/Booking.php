<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Booking extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'salon_id', 'user_id', 'service_id', 'start_time', 'end_time', 'status', 'payment_id', 'is_discount', 'promo_id'
    ];

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function promo()
    {
        return $this->belongsTo('');
    }

    /**
     * @param $value
     * @return Carbon
     */
    public function getStartTimeAttribute($value)
    {
        return new Carbon($this->attributes['start_time']);
    }

    /**
     * @param $value
     * @return Carbon
     */
    public function getEndTimeAttribute($value)
    {
        return new Carbon($this->attributes['end_time']);
    }

}

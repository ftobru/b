<?php

namespace App\Entities;

use App\References\RoutineReference;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Symfony\Component\Process\Exception\LogicException;

/**
 * Class Routine
 * @package App\Entities
 */
class Routine extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var array
     */
    protected $fillable = ['weekday', 'opening_time', 'closing_time', 'break_start', 'break_finish'];

    /**
     * @param $value
     */
    public function setWeekdayAttribute($value)
    {
        if(!in_array($value, RoutineReference::weekdays())) {
            throw new LogicException('Day of the week does not exist');
        }
        $this->attributes['weekday'] = $value;
    }
}

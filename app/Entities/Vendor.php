<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Vendor
 * @package App\Entities
 * @property string name
 * @property string email
 * @property string password
 * @property string phone
 * @property Carbon last_active
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Vendor extends Model implements Transformable, AuthenticatableContract
{
    use TransformableTrait, SoftDeletes, Authenticatable;

    protected $fillable = ['name', 'email', 'password', 'last_active', 'phone'];

    protected $hidden = ['password'];

    /**
     * @return Carbon
     */
    public function getLastActiveAttribute($value)
    {
        return new Carbon($value);
    }

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

    public function salons()
    {
        return $this->belongsToMany(Salon::class);
    }
}


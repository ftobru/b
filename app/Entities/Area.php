<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Area extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name', 'address', 'lat', 'lng'];
    
    public function salon()
    {
        return $this->hasOne(Salon::class);
    }

}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Setting
 * @package App\Entities
 */
class Setting extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['payment_types'];

    /**
     * @param $value
     * @return static
     */
    public function getPaymentTypesAttribute($value)
    {
        return Collection::make(
            $this->attributes['payment_types']
                ? json_decode($this->attributes['payment_types']) : null);
    }

    /**
     * @param $value
     */
    public function setPaymentTypesAttribute($value)
    {
        $this->attributes['payment_types'] = is_array($value) ? json_encode($value) : $value;
    }
}

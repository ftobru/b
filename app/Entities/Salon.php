<?php

namespace App\Entities;

use App\Exceptions\TypeNotFoundException;
use App\References\SalonReference;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use League\Flysystem\Exception;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Salon
 * @package App\Entities
 * @property string name
 * @property int vendor_id
 * @property string address
 * @property double lat
 * @property double lng
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Salon extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $fillable = ['name', 'vendor_id', 'address', 'lat', 'lng', 'type', 'description'];

    protected $appends = ['type_label'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo('App\Entities\Vendor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brands()
    {
        return $this->belongsToMany('App\Entities\Brand');
    }
    
    public function phone()
    {
        return $this->belongsToMany('App\Entities\Phone');
    }
    
    public function email()
    {
        return $this->belongsToMany('App\Entities\Email');
    }

    public function site()
    {
        return $this->belongsToMany('App\Entities\Site');
    }
    
    public function pictures()
    {
        return $this->hasOne(\App\Entities\Picture::class);
    }
    
    public function area()
    {
        return $this->hasMany('App\Entities\Area');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Entities\Comment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staffs()
    {
        return $this->hasMany(Staff::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function routines()
    {
        return $this->belongsToMany(\App\Entities\Routine::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
        return $this->hasMany(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function settings()
    {
        return $this->belongsToMany(Setting::class);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getTypeLabelAttribute($value)
    {
        return SalonReference::types()[$this->attributes['type']];
    }



    /**
     * @param $value
     * @throws TypeNotFoundException
     */
    public function setAttributeType($value)
    {
        if(in_array($value, SalonReference::types())) {
            $this->attributes['type'] = $value;
        } else {
            throw new TypeNotFoundException;
        }
    }



}

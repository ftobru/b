<?php

namespace App\Entities;

use App\Exceptions\TypeNotFoundException;
use App\References\SmsReference;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Sms
 * @package App\Entities
 */
class Sms extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'sms';

    protected $fillable = ['to', 'message', 'type', 'status', 'recipient_id', 'api_id', 'destination', 'errorCode'];

    /**
     * @param $value
     * @return mixed
     * @throws TypeNotFoundException
     */
    public function setTypeAttribute($value)
    {
        if(!in_array($value, SmsReference::types())) {
            throw new TypeNotFoundException;
        }
        return $this->attributes['type'] = $value;
    }
}
<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;


/**
 * Class Admin
 * @package App\Entities
 * @property string name
 * @property string password
 * @property string email
 * @property string phone
 * @property Carbon last_active
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_at
 */
class Admin extends Model implements Transformable, AuthenticatableContract
{
    use TransformableTrait, SoftDeletes, Authenticatable;

    protected $fillable = ['name', 'password', 'email', 'phone', 'last_active'];

    protected $hidden = ['password'];

    /**
     * @return Carbon
     */
    public function getLastActive()
    {
        return new Carbon($this->last_active);
    }


    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

}

<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RoutineStaffRepository;
use App\Entities\RoutineStaff;

/**
 * Class RoutineStaffRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RoutineStaffRepositoryEloquent extends BaseRepository implements RoutineStaffRepository
{


    protected $fieldSearchable = [
        'id',
        'salon_id',
        'staff_id',
        'weekday'
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoutineStaff::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

<?php

namespace App\Repositories\Eloquent;

use App\Contracts\Repositories\ServicesRepository;
use App\Entities\Service;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ServivesRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class ServicesRepositoryEloquent extends BaseRepository implements ServicesRepository
{

    protected $fieldSearchable = [
        'id',
        'salon_id',
        'price',
        'old_price',
        'duration',
        'name'
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Service::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

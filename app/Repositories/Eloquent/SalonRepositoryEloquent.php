<?php

namespace App\Repositories\Eloquent;

use App\Contracts\Repositories\SalonRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Salon;

/**
 * Class SalonRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SalonRepositoryEloquent extends BaseRepository implements SalonRepository
{

    protected $fieldSearchable = [
        'id',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Salon::class;
    }
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }


}
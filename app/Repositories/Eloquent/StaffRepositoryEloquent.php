<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\StaffRepository;
use App\Entities\Staff;

/**
 * Class StaffRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class StaffRepositoryEloquent extends BaseRepository implements StaffRepository
{

    protected $fieldSearchable = [
        'id',
        'salon_id'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Staff::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RoutineRepository;
use App\Entities\Routine;

/**
 * Class RoutineRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RoutineRepositoryEloquent extends BaseRepository implements RoutineRepository
{
    protected $fieldSearchable = [
        'id',
        'weekday',
        'status'
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Routine::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

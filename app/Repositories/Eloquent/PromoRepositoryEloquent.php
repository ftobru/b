<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PromoRepository;
use App\Entities\Promo;

/**
 * Class PromoRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class PromoRepositoryEloquent extends BaseRepository implements PromoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Promo::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

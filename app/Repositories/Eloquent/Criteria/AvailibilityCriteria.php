<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 29.10.15
 * Time: 20:19
 */

namespace App\Repositories\Eloquent\Criteria;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class AvailibilityCriteria implements CriteriaInterface
{
    /** @var  Carbon */
    protected $startDate;

    /** @var  Carbon */
    protected $endDate;

    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->with('bookings')->whereHas('bookings', function($q){
            $q->whereNotBetween(
                'bookings.start_time',
                [
                    $this->startDate->toDateTimeString(), $this->endDate->toDateTimeString()
                ]
            )->whereNotBetween(
                'bookings.end_time',
                [
                    $this->startDate->toDateTimeString(), $this->endDate->toDateTimeString()
                ]
            );
        });

    }

}
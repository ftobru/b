<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 03.11.15
 * Time: 15:50
 */

namespace App\Repositories\Eloquent\Criteria;


use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderPriceHigherCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->with(['services' => function($query){
            $query->orderBy('price', 'DESC');
        }]);
    }

}
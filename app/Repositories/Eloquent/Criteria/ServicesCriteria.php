<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 13.10.15
 * Time: 15:51
 */

namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ServiceTypeCriteria
 * @package App\Repositories\Eloquent\Criteria
 */
class ServicesCriteria implements CriteriaInterface
{
    protected $services;

    /**
     * @param array $services
     */
    public function __construct(array $services)
    {
        $this->services = $services;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->with('services')->whereHas('services', function($q){
            $q->whereIn('services.id', $this->services);
        });
    }

}
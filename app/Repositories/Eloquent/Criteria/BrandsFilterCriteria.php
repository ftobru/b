<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 13.10.15
 * Time: 16:00
 */

namespace App\Repositories\Eloquent\Criteria;


use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class BrandsFilterCriteria
 * @package App\Repositories\Eloquent\Criteria
 */
class BrandsFilterCriteria implements CriteriaInterface
{

    protected $brands;

    /**
     * @param array $brands
     */
    public function __construct(array $brands)
    {
        $this->brands = $brands;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->whereHas('brands', function($q){
            $q->whereIn('brands.id', $this->brands);
        });
    }

}
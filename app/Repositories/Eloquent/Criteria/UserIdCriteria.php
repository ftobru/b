<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 03.11.15
 * Time: 16:30
 */

namespace App\Repositories\Eloquent\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserIdCriteria implements CriteriaInterface
{
    protected $userId;

    public function __construct($userId)
    {
        $this->userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('user_id', '=', $this->userId);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 29.10.15
 * Time: 21:32
 */

namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class ServiceTypesCriteria
 * @package App\Repositories\Eloquent\Criteria
 */
class ServiceTypesCriteria implements CriteriaInterface
{
    protected $serverTypes;

    /**
     * @param array $serverTypes
     */
    public function __construct(array $serverTypes)
    {
        $this->serverTypes = $serverTypes;
    }
    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->with('services')->whereHas('services', function($q){
            $q->whereIn('services.service_type_id', $this->serverTypes);
        });
    }

}
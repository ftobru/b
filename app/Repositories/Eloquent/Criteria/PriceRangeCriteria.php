<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 29.10.15
 * Time: 21:59
 */

namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class PriceRangeCriteria implements CriteriaInterface
{
    protected $minPrice;

    protected $maxPrice;

    /**
     * @param $minPrice
     * @param $maxPrice
     */
    public function __construct($minPrice, $maxPrice)
    {
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->whereHas('services', function($q){
            $q->whereBetween('price', [$this->minPrice, $this->maxPrice]);
        });
    }

}
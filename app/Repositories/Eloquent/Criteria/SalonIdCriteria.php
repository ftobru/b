<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 11:00
 */

namespace App\Repositories\Eloquent\Criteria;


use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SalonCriteria
 * @package App\Repositories\Eloquent\Criteria
 */
class SalonIdCriteria implements  CriteriaInterface
{
    /** @var  int */
    protected $salonId;

    /**
     * @param int $salonId
     */
    public function __construct($salonId)
    {
        $this->salonId = $salonId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->where('salon_id', '=', $this->salonId);
    }

}

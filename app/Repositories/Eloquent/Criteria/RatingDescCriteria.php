<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 03.11.15
 * Time: 15:54
 */

namespace App\Repositories\Eloquent\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RatingDescCriteria
 * @package App\Repositories\Eloquent\Criteria
 */
class RatingDescCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Builder $model */
        return $model->orderBy('rating', 'DESC');
    }

}
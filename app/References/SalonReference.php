<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 21.09.15
 * Time: 13:43
 */

namespace App\References;

class SalonReference
{

    const TypeNotFound = 0;
    //
    const HairSalon = 21;
    //
    const HotelSpa = 1;
    //
    const DaySpa = 5;
    //
    const SkinClinic = 50;
    //
    //
    const AirportSpa = 28;
    //
    const Barbershop = 43;
    //
    const BeautyInstitute = 46;
    //
    const BeautySchool = 45;
    //
    const Bootcamp = 16;
    //
    const BrowBar = 47;
    //
    const CasinoSpa = 4;
    //
    const ChiropodyClinic = 20;
    //
    const ChiropracticClinic = 49;
    //
    const CruiseShipSpa = 36;
    //
    const DanceStudio = 35;
    //
    const DentalSpa = 7;
    //
    const DestinationSpa = 2;
    //
    const EyeClinic = 12;
    //
    const FatFarm = 18;
    //
    const FishSpa = 51;
    //
    const FitnessCentre = 9;
    //
    const GolfCourse = 24;
    //
    const Gym = 15;
    //
    const Hammam = 41;
    //
    const HealthClub = 59;
    //
    const HealthFarm = 29;
    //
    const MakeupStudio = 53;
    //
    const MartialArtsCentre = 38;
    //
    const MedicalSpa = 6;
    //
    const MobileBeauty = 42;
    //
    const MobileFitness = 14;
    //
    const MobileMassage = 57;
    //
    const MobileSpa = 8;
    //
    const NailSalon = 27;
    //
    const NutritionCentre = 55;
    //
    const OnlineRetailer = 58;
    //
    const PhotographicStudio = 60;
    //
    const PilatesStudio = 22;
    //
    const SurfSchool = 56;
    //
    const SwimmingPool = 25;
    //
    const TanningSalon = 37;
    //
    const TennisCourt = 23;
    //
    const TreatmentRoom  = 54;
    //
    const TreatmentRoomSpa  = 26;
    //
    const TreatmentRoomWellness  = 44;
    //
    const WaxingSalon = 40;
    //
    const WeightLossClinic = 52;
    //
    const WellnessCentre = 11;
    //
    const YogaRetreat = 17;
    //
    const YogaStudio = 13;


    public static function types()
    {
        return [
            self::HairSalon => 'Hair Salon',
            self::AirportSpa => 'Airport Spa',
            self::Barbershop => 'Barber shop',
            self::BeautyInstitute => 'Beauty Institute',
            self::BeautySchool => 'Beauty School',
            self::Bootcamp => 'Bootcamp',
            self::BrowBar => 'Browbar',
            self::CasinoSpa => 'Casino Spa',
            self::ChiropodyClinic => 'Chiropody Clinic',
            self::CruiseShipSpa => 'Cruise Ship Spa',
            self::DanceStudio => 'Dance Studio',
            self::DentalSpa => 'Dental Spa',
            self::DaySpa => 'Day Spa',
            self::DestinationSpa => 'Destination Spa',
            self::EyeClinic => 'Eye Clinic',
            self::FatFarm => 'Fat Farm',
            self::FishSpa => 'Fish Spa',
            self::FitnessCentre => 'Fitness Center',
            self::ChiropracticClinic => 'Chiropractic Clinic',
            self::YogaStudio => 'Yoga Studio',
            self::WellnessCentre => 'Wellness Centre',
            self::WeightLossClinic => 'Weight Loss Clinic',
            self::WaxingSalon => 'Waxing Salon',
            self::TreatmentRoomWellness => 'Treatment Room Wellness',
            self::TreatmentRoomSpa => 'Treatment Room Spa',
            self::TennisCourt => 'TennisCourt',
            self::TanningSalon => 'Tanning Salon',
            self::SwimmingPool => 'Swimming Pool',
            self::SurfSchool => 'Surf School',
            self::SkinClinic => 'Skin Clinic',
            self::PilatesStudio => 'Pilates Studio',
            self::OnlineRetailer => 'Online Retailer',
            self::NutritionCentre => 'Nutrition Centre',
            self::HotelSpa => 'Hotel Spa',
            self::NailSalon => 'Nail Salon',
            self::YogaRetreat => 'Yoga Salon',
            self::TypeNotFound => 'Type Not Found'
        ];
    }

    public static function typesId()
    {
        return [
            self::HairSalon,
            self::AirportSpa ,
            self::Barbershop ,
            self::BeautyInstitute,
            self::BeautySchool,
            self::Bootcamp,
            self::BrowBar,
            self::CasinoSpa,
            self::ChiropodyClinic,
            self::CruiseShipSpa,
            self::DanceStudio,
            self::DentalSpa,
            self::DaySpa,
            self::DestinationSpa,
            self::EyeClinic,
            self::FatFarm,
            self::FishSpa,
            self::FitnessCentre,
            self::ChiropracticClinic,
            self::YogaStudio,
            self::WellnessCentre,
            self::WeightLossClinic,
            self::WaxingSalon,
            self::TreatmentRoomWellness,
            self::TreatmentRoomSpa,
            self::TennisCourt,
            self::TanningSalon,
            self::SwimmingPool,
            self::SurfSchool,
            self::SkinClinic,
            self::PilatesStudio,
            self::OnlineRetailer,
            self::NutritionCentre,
            self::HotelSpa,
            self::NailSalon,
            self::YogaRetreat,
            self::TypeNotFound
        ];
    }
}
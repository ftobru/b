<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 17:15
 */

namespace App\References;


use App\References\Interfaces\StatusInterface;

class CommentReference implements StatusInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_BLOCK = -1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_BLOCK,
            self::STATUS_NOT_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }


}
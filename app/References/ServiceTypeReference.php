<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 21:22
 */

namespace App\References;

/**
 * Class ServiceTypeReference
 * @package App\References
 */
class ServiceTypeReference
{
    const Hair = 1;

    const Makeup = 2;

    const Nails = 3;

    const BrowAndLash = 4;

    const HennaTattooArtist = 5;

    const Facial = 5;

    const BodyTheraphy = 6;

    const Spa = 7;

    const WaxingHairRemoval = 8;

    const Personal = 9;

    const Training = 10;

    const Massage = 11;

    const Yoga = 12;

    const Sports = 13;

    const MartialArts = 14;

    const Aesthethics = 15;

    const Photography = 16;

    const Wedding = 17;

    const EyeTreatment = 18;

    const Stylist = 19;

    const Dance = 20;

    public function types()
    {
        return [
            self::Hair => 'Hair',
            self::Makeup => 'Makeup',
            self::Nails => 'Nails',
            self::BrowAndLash => 'Brow & Lash',
            self::HennaTattooArtist => 'Henna / Tattoo Artist',
            self::Facial => 'Facial',
            self::BodyTheraphy => 'Body Theraphy',
            self::Spa => 'Spa',
            self::WaxingHairRemoval => 'Waxing / Hair Removal',
            self::Training => 'Personal Training',
            self::Massage => 'Massage',
            self::Yoga => 'Yoga',
            self::Sports => 'Sports',
            self::MartialArts => 'Martial Arts',
            self::Aesthethics => 'Aesthethics',
            self::Stylist => 'Stylist',
            self::Photography => 'Photography',
            self::Dance => 'Dance',
            self::Wedding => 'Wedding',
            self::EyeTreatment => 'Eye Treatment'
        ];
    }

}
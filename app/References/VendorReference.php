<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 17:12
 */

namespace App\References;


use App\References\Interfaces\StatusInterface;

/**
 * Class AdminReference
 * @package App\References
 */
class VendorReference implements StatusInterface
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NOT_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

}
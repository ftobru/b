<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 14:26
 */

namespace App\References;

/**
 * Class SmsReference
 * @package App\References
 */
class SmsReference
{
    const TYPE_ADMIN = 0;

    const TYPE_VENDOR = 1;

    const TYPE_USER = 2;

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_ADMIN,
            self::TYPE_VENDOR,
            self::TYPE_USER
        ];
    }
}
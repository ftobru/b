<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 23.10.15
 * Time: 15:01
 */

namespace App\References;

/**
 * Class RoutineReference
 * @package App\References
 */
class RoutineReference
{
    const STATUS_WORK = 0;

    const STATUS_NOT_WORK = 1;

    /**
     * @return array
     */
    public static function weekdays()
    {
        return ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    }

    public static function statuses()
    {
        return [
            self::STATUS_NOT_WORK,
            self::STATUS_WORK
        ];
    }
}
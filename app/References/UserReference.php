<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 20:01
 */

namespace App\References;

/**
 * Class UserReference
 * @package App\References
 */
class UserReference
{
    const STATUS_NOT_ACTIVE = 0;

    const STATUS_ACTIVE = 1;

    const STATUS_BLOCK = 2;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NOT_ACTIVE,
            self::STATUS_ACTIVE,
            self::STATUS_NOT_ACTIVE
        ];
    }
}
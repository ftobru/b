<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 24.10.15
 * Time: 16:37
 */

namespace App\References;


class BookingReference
{
    const STATUS_NEW = 0;
    const STATUS_CONFIRMED = 1;

    public function statuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_CONFIRMED
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 17:11
 */

namespace App\References\Interfaces;

/**
 * Interface StatusInterface
 * @package App\References\Interfaces
 */
interface StatusInterface
{
    /**
     * @return array
     */
    public static function statuses();
}
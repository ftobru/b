<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 16:32
 */

namespace App\References;

class PromoReference
{
    const STATUS_ACTIVE = 0;

    const STATUS_USED = 1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_USED
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 22.08.15
 * Time: 17:58
 * \App\Api\V1\Providers
 */

namespace App\Api\V1\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ApiRouteServiceProvider extends ServiceProvider {

    protected $namespace = 'App\Api\V1\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  Router  $router
     * @return void
     */
    public function boot(Router $router) {
        //

        parent::boot($router);
    }

    public function map(\Dingo\Api\Routing\Router $api) {

        $api->version(['version' => 'v1'], function($api) {
            return require app_path('Api/V1/Http/routes.php');
        });
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 14:46
 */

namespace App\Api\V1\Http\Middleware;


use Auth;
use Illuminate\Session\Store;
use Ollieread\Multiauth\Guard;

class SalonOwnerMiddleware
{
    /** @var Store  */
    protected $store;

    /** @var  Guard */
    protected $auth;

    /**
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
        $this->auth = Auth::vendor();

    }

    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if(!$this->store->get('salon_id')) {
            if($this->auth->get()) {
                if($salon_id = object_get($this->auth->get()->salons(), 'id', null)) {
                    $this->store->set('salon_id', $salon_id);
                }
            }
        }

        return $next($request);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 14:39
 */

namespace App\Api\V1\Http\Middleware;

use Auth;
use Ollieread\Multiauth\Guard;

class AuthenticateUserMiddleware
{
    /** @var  Guard */
    protected $auth;

    public function __construct()
    {
        $this->auth = Auth::user();
    }

    public function handle($request, \Closure $next)
    {
        if ($this->auth->guest()) {
//            if ($request->ajax()) {
                return response('Unauthorized.', 403);
//            } else {
//                return redirect()->guest('/');
//            }
        }
        return $next($request);
    }
}
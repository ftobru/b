<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 14:39
 */

namespace App\Api\V1\Http\Middleware;

use Auth;
use Ollieread\Multiauth\Guard;

class AuthenticateVendorMiddleware
{
    /** @var  Guard */
    protected $auth;

    public function __construct()
    {
        $this->auth = Auth::vendor();
    }

    public function handle($request, \Closure $next)
    {
        if ($this->auth->guest()) {
            return response('Unauthorized.', 403);
        }
        return $next($request);
    }
}
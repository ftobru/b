<?php
use Dingo\Api\Routing\Router;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app(Router::class);

$api->get('hand', ['as' => 'api.hand', 'uses' => 'App\Api\V1\Http\Controllers\HandController@index']);
//$api->group(['middleware' => 'api.auth'], function($api) {
/** @var Dingo\Api\Routing\Router $api */
$api->post('/vendor/registration', 'App\Api\V1\Http\Controllers\VendorRegistrationController@store');
$api->resource('vendors', App\Api\V1\Http\Controllers\VendorController::class);
$api->resource('users', App\Api\V1\Http\Controllers\UserController::class);
$api->resource('salons', App\Api\V1\Http\Controllers\SalonController::class);
$api->resource('brands', App\Api\V1\Http\Controllers\BrandController::class);
$api->resource('admins', App\Api\V1\Http\Controllers\AdminController::class);
$api->resource('comments', App\Api\V1\Http\Controllers\CommentController::class);
$api->resource('salonsTypes', App\Api\V1\Http\Controllers\SalonTypesController::class);
$api->resource('area', App\Api\V1\Http\Controllers\AreaController::class);
$api->resource('staffs', App\Api\V1\Http\Controllers\StaffController::class, ['middleware' => 'vendor-owner']);
$api->resource('services', App\Api\V1\Http\Controllers\ServicesController::class, ['middleware' => 'vendor-owner']);
$api->resource('user/booking', App\Api\V1\Http\Controllers\UserBookingController::class);
$api->resource('vendor/booking', App\Api\V1\Http\Controllers\VendorBookingController::class);
$api->resource('pictures', App\Api\V1\Http\Controllers\PictureController::class);
$api->resource('serviceTypes', App\Api\V1\Http\Controllers\ServiceTypesController::class);
$api->resource('payment/token', App\Api\V1\Http\Controllers\PaymentTokenController::class);
$api->resource('payment/pay', App\Api\V1\Http\Controllers\PaymentPayController::class);
$api->resource('payment/payCash', App\Api\V1\Http\Controllers\PaymentCashController::class);
$api->resource('routineStaffs', App\Api\V1\Http\Controllers\RoutineStaffController::class);
$api->resource('reservation', App\Api\V1\Http\Controllers\ReservationsController::class);
$api->resource('routines', App\Api\V1\Http\Controllers\RoutineController::class);
$api->resource('salonsFilter', App\Api\V1\Http\Controllers\FilterSalonsController::class);
$api->resource('smsCallback/QWkbAEnKdMvn', App\Api\V1\Http\Controllers\SmsCallbackController::class);
$api->post('/login', ['as' => 'api.login', 'uses' => 'App\Api\V1\Http\Controllers\LoginController@login']);
//});


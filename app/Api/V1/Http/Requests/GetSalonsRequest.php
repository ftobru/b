<?php
namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class GetSalonsRequest
 * @package App\Api\V1\Http\Requests
 */
class GetSalonsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }


}


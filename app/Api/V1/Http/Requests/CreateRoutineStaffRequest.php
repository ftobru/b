<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 29.10.15
 * Time: 23:02
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

class CreateRoutineStaffRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'weekday' => 'required',
            'work_time_start' => 'required',
            'work_time_end' => 'required',
            'staff_id' => 'required|exists:staffs,id'
        ];
    }
}
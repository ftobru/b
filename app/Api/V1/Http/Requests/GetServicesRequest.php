<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 18:25
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class GetServicesRequest
 * @package App\Api\V1\Http\Requests
 */
class GetServicesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }
}
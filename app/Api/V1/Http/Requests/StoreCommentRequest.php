<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 03.11.15
 * Time: 16:59
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class StoreCommentRequest
 * @package App\Api\V1\Http\Requests
 */
class StoreCommentRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string',
            'salon_id' => 'required|exists:salons,id'
        ];
    }
}
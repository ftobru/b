<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 11:12
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

class UpdateRoutineStaffRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'work_time_start' => 'required',
            'work_time_end' => 'required'
        ];
    }
}
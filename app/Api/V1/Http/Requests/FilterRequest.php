<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 13.10.15
 * Time: 19:59
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class FilterRequest
 * @package App\Api\V1\Http\Requests
 */
class FilterRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'limit' => 'numeric',
            'availibilityDate' => 'string',
            'availibilityTimeStart' => 'string',
            'availibilityTimeEnd' => 'string',
            'types' => 'array',
            'services' => 'array',
            'priceRangeMin' => 'numeric',
            'priceRangeMax' => 'numeric',
            'brands' => 'array',
            'sortPriceLower' => 'string',
            'sortPriceHigher' => 'string',
            'sortRating' => 'string'
        ];
    }
}

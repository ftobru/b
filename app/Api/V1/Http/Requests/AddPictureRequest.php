<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 11:05
 */

namespace App\Api\V1\Http\Requests;



use App\Http\Requests\Request;

class AddPictureRequest extends Request
{
    public function rules()
    {
        return [
            'pictures' => 'array'
        ];
    }

}
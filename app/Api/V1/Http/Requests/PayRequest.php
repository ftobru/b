<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.10.15
 * Time: 14:24
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;


/**
 * Class PayRequest
 * @package App\Api\V1\Http\Requests
 */
class PayRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'payment_method_nonce' => 'required',
            'fullName' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'promo' => 'exists:promo,code',
            'salon_id' => 'required|exists:salons,id',
            'service_id' => 'required|exists:services,id',
            'staff_id' => 'required|exists:staffs,id',
            'startTime' => 'required|string'
        ];
    }
}

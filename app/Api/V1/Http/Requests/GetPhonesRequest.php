<?php
namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class GetPhonesRequest
 * @package App\Api\V1\Http\Requests
 */
class GetPhonesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }


}


<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 19:30
 */

namespace App\Api\V1\Http\Requests;

use Request;

/**
 * Class GetBookingRequest
 * @package App\Api\V1\Http\Requests
 */
class GetBookingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric',
            'from_date' => 'date',
            'to_date' => 'date'
        ];
    }
}
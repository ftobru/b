<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.10.15
 * Time: 17:03
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

class CreateServiceRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|max:255',
            'duration' => 'required',
            'price' => 'required',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
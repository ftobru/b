<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.10.15
 * Time: 18:52
 */

namespace App\Api\V1\Http\Requests;


use App\Http\Requests\Request;

class CreateRoutineRequest extends Request
{
    public function rules()
    {
        return [
            'weekday' => 'required',
            'opening_time' => 'required',
            'closing_time' => 'required',
            'break_start' => 'required',
            'break_finish' => 'required'
        ];
    }
}
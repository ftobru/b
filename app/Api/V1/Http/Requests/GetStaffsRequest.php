<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 18:18
 */

namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

class GetStaffsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }
}
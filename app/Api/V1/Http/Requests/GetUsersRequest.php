<?php
namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class GetUsersRequest
 * @package App\Api\V1\Http\Requests
 */
class GetUsersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }


}


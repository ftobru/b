<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:23
 */
namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class AdminLoginRequest
 * @package App\Api\V1\Http\Requests
 */
class AdminLoginRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required'
        ];
    }
}
<?php
namespace App\Api\V1\Http\Requests;


use App\Http\Requests\Request;

/**
 * Class LoginRequest
 * @package App\Api\V1\Http\Requests
 */
class LoginRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone' => 'required',
            'password' => 'required'
        ];
    }
}
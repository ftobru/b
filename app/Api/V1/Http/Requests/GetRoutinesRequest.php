<?php
namespace App\Api\V1\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class GetVendorsRequest
 * @package App\Api\V1\Http\Requests
 */
class GetRoutinesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'numeric'
        ];
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 21:36
 */

namespace App\Api\V1\Http\Controllers;


use App\References\ServiceTypeReference;
use Illuminate\Support\Collection;

class ServiceTypesController extends ApiController
{
    /**
     * @var ServiceTypeReference
     */
    protected $reference;

    public function __construct()
    {
        $this->reference = new ServiceTypeReference();
    }

    public function index()
    {
        return [
            'data' => Collection::make($this->reference->types())->map(function($item, $key){
                return ['id'=> $key, 'name'=> $item];
            })->values()
        ];
    }
}
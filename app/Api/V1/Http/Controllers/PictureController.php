<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 10:53
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Controllers\ApiController;
use App\Api\V1\Http\Requests\AddPictureRequest;
use App\Api\V1\Http\Requests\GetPicturesRequest;
use App\Contracts\Repositories\PictureRepository;
use App\Exceptions\SalonNotFoundException;
use App\Repositories\Eloquent\Criteria\SalonIdCriteria;
use App\Repositories\Eloquent\Eloquent\PictureRepositoryEloquent;
use App\Services\AddPictureService;
use App\Services\ApplicationService;

class PictureController extends ApiController
{
    /** @var PictureRepository|PictureRepositoryEloquent  */
    protected $repository;

    protected $createrService;

    /** @var ApplicationService  */
    protected $applicationService;

    /**
     * @param PictureRepository $repository
     * @param ApplicationService $applicationService
     * @param AddPictureService $addPictureService
     */
    public function __construct(
        PictureRepository $repository,
        ApplicationService $applicationService,
        AddPictureService $addPictureService
    )
    {
        $this->repository = $repository;
        $this->applicationService = $applicationService;
        $this->createrService = $addPictureService;
    }

    /**
     * @param GetPicturesRequest $request
     * @return mixed
     * @throws SalonNotFoundException
     */
    public function index(GetPicturesRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }

    public function store(AddPictureRequest $request)
    {

    }
}
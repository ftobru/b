<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:22
 */
namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\AdminLoginRequest;
use App\Services\AdminLoginService;

/**
 * @Resource("Admin login", uri="/admin/login")
 * Class AdminLoginController
 * @package App\Api\V1\Http\Controllers
 */
class AdminLoginController extends ApiController
{
    protected $service;

    /**
     * @param AdminLoginService $service
     */
    public function __construct(AdminLoginService $service)
    {
        $this->service = $service;
    }

    /**
     * @Put("/")
     * @param AdminLoginRequest $request
     * @return bool
     */
    public function login(AdminLoginRequest $request)
    {
        return $this->service->login($request->get('email'), $request->get('password'));
    }
}
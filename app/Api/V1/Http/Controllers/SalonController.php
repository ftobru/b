<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\GetSalonsRequest;
use App\Contracts\Repositories\SalonRepository;
use Dingo\Blueprint\Annotation\Response;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class SalonController
 * @package App\Api\V1\Http\Controllers
 * @Resource("Salons", uri="/salons")
 */
class SalonController extends ApiController
{
    /**
     * @var SalonRepository
     */
    protected $salonRepository;

    /**
     * @param SalonRepository $salonRepository
     *
     */
    public function __construct(SalonRepository $salonRepository)
    {
        $this->middleware('auth.vendor', ['only' => ['store']]);
        $this->middleware('owner.salon');
        $this->salonRepository = $salonRepository;
    }

    /**
     * @Get("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("limit", description="The page result to view")
     * })
     * @Transaction({
     *      @Request({"limit": 5, "orderBy": "id", "sortedBy": "desc", "filter": "id;name", "search": "id:1"}),
     *      @Response(200, body={
     *              "total": 1,
     *              "per_page": "10",
     *              "current_page": 1,
     *              "last_page": 1,
     *              "next_page_url": null,
     *              "prev_page_url": null,
     *              "from": 1,
     *              "to": 6,
     *              "data":
     *                  {
     *                      "id": 1,
     *                      "status": 0,
     *                      "name": "Zora Murphy",
     *                      "vendor_id": 1,
     *                      "address": "80120 Ulises Springs Suite 918\nAliviaview, NC 63411-5291",
     *                      "lat": "140.3073",
     *                      "lng": "2.081",
     *                      "created_at": "2015-09-11 11:56:33",
     *                      "updated_at": "2015-09-11 11:56:33",
     *                      "deleted_at": null,
     *                      "rating": 0
     *                   }
     *      })
     * })
     * Display a listing of the resource.
     * @param GetSalonsRequest $request
     * @return Response
     */
    public function index(GetSalonsRequest $request)
    {
        return $this->salonRepository->paginate($request->get('limit', null));
    }


    /**
     * Store a newly created resource in storage.
     * @Post("/")
     * @Version({"v1"})
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->salonRepository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->salonRepository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->salonRepository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->salonRepository->delete($id)];
    }
}

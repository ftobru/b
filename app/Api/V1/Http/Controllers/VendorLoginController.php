<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:16
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\LoginRequest;
use App\Services\VendorLoginService;
use Auth;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

class VendorLoginController extends ApiController
{
    protected $service;

    /**
     * @param VendorLoginService $service
     */
    public function __construct(VendorLoginService $service)
    {
        $this->service = $service;
    }

    /**
     * @param LoginRequest $request
     * @return bool
     */
    public function login(LoginRequest $request)
    {
        if($this->service->login($request->get('phone'), $request->get('password'))) {
            return Auth::vendor()->get();
        } else {
            abort(401, 'Access denied');
        }
    }

}
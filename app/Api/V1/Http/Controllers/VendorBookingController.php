<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 19:24
 */


namespace App\Api\V1\Http\Controllers;

use App\Contracts\Repositories\BookingRepository;
use App\Repositories\Eloquent\BookingRepositoryEloquent;
use Request;

class VendorBookingController extends ApiController
{
    /**
     * @var BookingRepositoryEloquent
     */
    protected $repository;


    public function __construct(BookingRepository $repository)
    {
        $this->middleware('auth.vendor');
        $this->middleware('owner.salon');
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }

}
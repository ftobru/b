<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.10.15
 * Time: 14:22
 */

namespace App\Api\V1\Http\Controllers;


use App\Api\V1\Http\Requests\PayRequest;
use App\Contracts\Repositories\PromoRepository;
use App\Contracts\Repositories\UserRepository;
use App\Entities\Payments;
use App\Entities\User;
use App\Exceptions\PromoCodeNotFoundException;
use App\ObjectValues\BookingObject;
use App\ObjectValues\PaymentObject;
use App\ObjectValues\UserObject;
use App\Services\AutoRegistrationUserService;
use App\Services\PaymentService;
use App\Services\PromoService;
use App\Services\ServiceBookingService;

class PaymentPayController extends ApiController
{
    /** @var PaymentService  */
    protected  $paymentService;
    /** @var  AutoRegistrationUserService */
    protected $registrationUserService;

    /** @var  PromoRepository */
    protected $promoRepository;
    /** @var  PromoService */
    protected $promoService;
    /** @var  ServiceBookingService */
    protected $serviceBookingService;
    /** @var UserRepository  */
    protected $userRepository;

    /**
     * @param PaymentService $paymentService
     * @param AutoRegistrationUserService $autoRegistrationUserService
     * @param PromoRepository $promoRepository
     * @param PromoService $promoService
     * @param ServiceBookingService $bookingService
     * @param UserRepository $userRepository
     */
    public function __construct(
        PaymentService $paymentService,
        AutoRegistrationUserService $autoRegistrationUserService,
        PromoRepository $promoRepository,
        PromoService $promoService,
        ServiceBookingService $bookingService,
        UserRepository $userRepository
    )
    {
        $this->registrationUserService = $autoRegistrationUserService;
        $this->paymentService = $paymentService;
        $this->promoRepository = $promoRepository;
        $this->promoService = $promoService;
        $this->serviceBookingService = $bookingService;
        $this->userRepository = $userRepository;

    }

    /**
     * @param PayRequest $request
     * @return Payments
     * @throws PromoCodeNotFoundException
     * @throws \App\Exceptions\UserAutoRegistrationException
     * @throws \App\Exceptions\UserNotFoundException
     */
    public function store(PayRequest $request)
    {
        if(!($user = $this->userRepository->findByField('phone', $request->get('phone'))->first())) {
            /** @var User $user */
            $user = $this->registrationUserService->registration(new UserObject(
                $request->get('email'), $request->get('phone'), $request->get('fullName')
            ));
        }


        // Promo code
        /** @var  $promoId */
        $promoId = null;
        if($request->get('promo')) {
            $promo = $this->promoRepository->findByField('code', $request->get('promo'));
            if(!$promo) {
                throw new PromoCodeNotFoundException;
            }
            $promoId = $promo->id;
        }

        $paymentObject = new PaymentObject(
            $user->id,
            $request->get('amount'),
            $request->get('payment_method_nonce'),
            $request->get('service_id'),
            $request->get('salon_id'),
            $promoId
        );
        /** @var Payments $payment */
        $payment = $this->paymentService->payment($paymentObject);

        $bookingObject = new BookingObject(
            $paymentObject->getSalonId(),
            $paymentObject->getUserId(),
            $request->get('staff_id'),
            $paymentObject->getServiceId(),
            $request->get('startTime'),
            $payment->id,
            $promoId
        );

        $booking = $this->serviceBookingService->booking($bookingObject);
        if($booking && $payment && $promoId) {
            $this->promoService->apply($promoId);
        }

        return $payment;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 17:05
 */

namespace App\Api\V1\Http\Controllers;


use Clickatell\Callback;
use Illuminate\Http\Request;
use Log;

class SmsCallbackController extends ApiController
{

    public function store(Request $request)
    {
        Callback::parseCallback(function($values){
            Log::useFiles(storage_path('logs/sms_callback'), 'info');
            Log::info('Sms callback --> ', $values);
        });
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 13:18
 */

namespace App\Api\V1\Http\Controllers;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

abstract class ApiController extends Controller
{
    use Helpers;
}
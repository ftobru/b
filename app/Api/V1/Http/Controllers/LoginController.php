<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:16
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\LoginRequest;
use App\Services\UserLoginService;
use App\Services\VendorLoginService;
use Auth;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

class LoginController extends ApiController
{
    /** @var VendorLoginService  */
    protected $vendorService;

    protected $userLoginService;

    /**
     * @param VendorLoginService $vendorService
     * @param UserLoginService $userLoginService
     * @internal param VendorLoginService $service
     */
    public function __construct(VendorLoginService $vendorService, UserLoginService $userLoginService)
    {
        $this->vendorService = $vendorService;
        $this->userLoginService = $userLoginService;
    }

    /**
     * @param LoginRequest $request
     * @return bool
     */
    public function login(LoginRequest $request)
    {
        if($this->vendorService->login($request->get('phone'), $request->get('password'))) {
            return ['vendor' => Auth::vendor()->get()];
        } else if($this->userLoginService->login($request->get('phone'), $request->get('password'))) {
            return ['user' => Auth::user()->get()];
        } else {
            abort(401, 'Access denied');
        }
    }

}
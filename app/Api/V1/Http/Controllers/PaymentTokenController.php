<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.10.15
 * Time: 14:19
 */

namespace App\Api\V1\Http\Controllers;

use App\Services\PaymentService;

class PaymentTokenController extends ApiController
{
    /** @var PaymentService  */
    protected $paymentService;

    /**
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService =  $paymentService;
    }

    /**
     * @return array
     */
    public function index()
    {
        return ['token' => $this->paymentService->generateToken()];
    }


}
<?php

namespace App\Api\V1\Http\Controllers;




use App\Api\V1\Http\Requests\GetCommentsRequest;
use App\Api\V1\Http\Requests\StoreCommentRequest;
use App\Contracts\Repositories\CommentRepository;
use App\Repositories\Eloquent\CommentRepositoryEloquent;
use App\Repositories\Eloquent\Criteria\SalonIdCriteria;
use Auth;
use Illuminate\Http\Request;
use Dingo\Blueprint\Annotation\Response;

use App\Http\Requests;


/**
 *
 * Class CommentController
 * @Resource("Comments", uri="/comments")
 * @package App\Api\V1\Http\Controllers
 */
class CommentController extends ApiController
{
    /**
     * @var CommentRepository|CommentRepositoryEloquent
     */
    protected $repository;

    /**
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->middleware('auth.user', ['only' => ['store']]);
        $this->repository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetCommentsRequest $request
     * @return Response
     */
    public function index(GetCommentsRequest $request)
    {
        return $this->repository
            ->pushCriteria(new SalonIdCriteria($request->get('salon_id')))
            ->paginate($request->get('limit'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCommentRequest  $request
     * @return Response
     */
    public function store(StoreCommentRequest $request)
    {
        $commentData = $request->all();
        $commentData = array_merge($commentData, ['user_id' => Auth::getUser()->id]);
        return $this->repository->create($commentData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}

<?php

namespace App\Api\V1\Http\Controllers;




use App\Api\V1\Http\Requests\GetBrandsRequest;
use App\Contracts\Repositories\BrandRepository;
use Illuminate\Http\Request;
use Dingo\Blueprint\Annotation\Response;

use App\Http\Requests;

/**
 * Class BrandController
 * @Resource("Brands", uri="/brands")
 * @package App\Api\V1\Http\Controllers
 */
class BrandController extends ApiController
{
    /**
     * @var BrandRepository
     */
    protected $repository;

    /**
     * @param BrandRepository $brandRepository
     */
    public function __construct(BrandRepository $brandRepository)
    {
        $this->middleware('auth.vendor', ['only' => ['store']]);
        $this->middleware('owner.salon');
        $this->repository = $brandRepository;
    }

    /**
     * Display a listing of the resource.
     * @Get("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("limit", description="The page result to view")
     * })
     * @Transactions({
     *      @Request({"limit": 5, "orderBy": "id", "sortedBy": "desc", "filter": "id;name", "search": "id:1"}),
     * })
     * @param GetBrandsRequest $request
     * @return Response
     */
    public function index(GetBrandsRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     * @Post("/")
     * @Version({"v1"})
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     * @Get("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     * @Put("/:id")
     * @Version({"v1"})
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     * @Delete("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}

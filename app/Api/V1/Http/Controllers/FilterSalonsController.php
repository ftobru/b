<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 12.10.15
 * Time: 17:35
 */


namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\FilterRequest;
use App\Services\FilterSalonService;

/**
 * Class FilterSalonsController
 * @package App\Api\V1\Http\Controllers
 */
class FilterSalonsController extends ApiController
{

    /** @var FilterSalonService  */
    protected $service;

    public function __construct(FilterSalonService $filterSalonService)
    {
        $this->service = $filterSalonService;
    }

    /**
     * @param FilterRequest $request
     * @return mixed
     */
    public function index(FilterRequest $request)
    {
        if($request->get('sortPriceLower')) {
            $this->service->sortByPriceLower();
        } else if ($request->get('sortPriceHigher')){
            $this->service->sortByPriceUpper();
        } else if ($request->get('sortRating')) {
            $this->service->sortByRating();
        }

        return $this->service->setAvailibility(
                $request->get('availibilityDate'),
                $request->get('availibilityTimeStart'),
                $request->get('availibilityTimeEnd')
            )->setServiceTypes($request->get('types', []))
            ->setServices($request->get('services', []))
            ->setPriceRange($request->get('priceRangeMin'), $request->get('priceRangeMax'))
            ->setBrands($request->get('brands', []))->all($request->get('limit'));
    }

}
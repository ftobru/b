<?php
namespace App\Api\V1\Http\Controllers;


use App\Api\V1\Http\Requests\LoginRequest;
use App\Services\UserLoginService;
use Helper\Api;

/**
 * @Resource("/user/login")
 * Class UserLoginController
 * @package App\Api\V1\Http\Controllers
 */
class UserLoginController extends Api
{
    protected $service;

    /**
     * @param UserLoginService $service
     */
    public function __construct(UserLoginService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * @Put("/")
     * @param LoginRequest $request
     * @return bool
     */
    public function login(LoginRequest $request)
    {
        return $this->service->auth($request->get('phone'), $request->get('password'));
    }
}
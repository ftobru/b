<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 19:19
 */

namespace App\Api\V1\Http\Controllers;

use App\Contracts\Repositories\BookingRepository;
use App\Repositories\Eloquent\BookingRepositoryEloquent;
use App\Repositories\Eloquent\Criteria\UserIdCriteria;
use Auth;

class UserBookingController extends ApiController
{
    /** @var BookingRepository|BookingRepositoryEloquent  */
    protected $repository;
    /** @var \App\User|null  */
    protected $user;

    public function __construct(BookingRepository $repository)
    {
        $this->middleware('auth.user', ['only' => ['index']]);
        $this->repository = $repository;
        $this->user = Auth::getUser();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->pushCritera(new UserIdCriteria($this->user->id))->paginate();
    }

}
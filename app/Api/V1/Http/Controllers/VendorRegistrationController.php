<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 15:47
 */

namespace App\Api\V1\Http\Controllers;

use App\Services\VendorRegistrationService;
use App\Transformers\VendorTransformer;
use App\Validators\VendorRegistrationValidator;
use Dingo\Api\Exception\StoreResourceFailedException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;

/**
 * Class VendorRegistrationController
 * @package App\Api\V1\Http\Controllers
 */
class VendorRegistrationController extends ApiController
{
    /** @var  VendorRegistrationService */
    protected $registrationService;
    /** @var VendorRegistrationValidator  */
    protected $validator;

    public function __construct(VendorRegistrationService $registrationService, VendorRegistrationValidator $validator)
    {
        $this->registrationService = $registrationService;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     *
     * @Post
     * @Versions({"v1"})
     * @Transaction({
     *      @Request({"vendor":{"name": "foo", "email": "foo[]example.com", "phone": "+791345500xx"}, "salon": {"name": "bar", "address": "st. Sivashskay h14", "lat": 111.1123, "lng": -12.1244, "type": 1, "postcode": "127001", "booking": "Test"  }}),
     *      @Response(200, body={"id":10, "status": 0, "type": 0, "name":"foo", "email":"foo[]example.com", "created_at": "2015-01-01 00:00:00", "updated_at": "2015-01-01 00:00:00"}),
     *      @Response(422, body={"message": "Fail registration vendor", "errors": "array"})
     * })
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail();
            return $this->response()->item($this->registrationService->registration(
                $request->all()),
                new VendorTransformer()
            );
        } catch(ValidatorException $e) {
            throw new StoreResourceFailedException('Fail registration vendor', $e->getMessage());
        }
    }
}
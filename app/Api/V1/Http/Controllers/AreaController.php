<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 21.09.15
 * Time: 18:44
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\GetUsersRequest;
use App\Contracts\Repositories\AreaRepository;

use App\Http\Requests;

/**
 * Class AreaController
 * @package App\Api\V1\Http\Controllers
 */
class AreaController extends ApiController
{
    protected $repository;

    public function __construct(AreaRepository $repository)
    {
        $this->repository = $repository;
    }


    public function index(GetUsersRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 21.09.15
 * Time: 18:44
 */

namespace App\Api\V1\Http\Controllers;

use App\References\SalonReference;
use Illuminate\Support\Collection;


/**
 * @Resources("Salon types", uri="/salons/types")
 * Class SalonTypesController
 * @package App\Api\V1\Http\Controllers
 */
class SalonTypesController extends ApiController
{
    protected $reference;

    /**
     * @param SalonReference $reference
     */
    public function __construct(SalonReference $reference)
    {
        $this->reference = $reference;
    }

    /**
     * @Get("/")
     * @return array
     */
    public function index()
    {
        return [
            'data' => Collection::make($this->reference->types())->map(function($item, $key){
                return ['id'=> $key, 'name'=> $item];
            })->values()
        ];
    }
}
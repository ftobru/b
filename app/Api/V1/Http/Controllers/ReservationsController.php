<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 02.11.15
 * Time: 20:14
 */

namespace App\Api\V1\Http\Controllers;


use App\Contracts\Repositories\ReservationRepository;
use Illuminate\Http\Request;


/**
 * Class ReservationsController
 * @package App\Api\V1\Http\Controllers
 */
class ReservationsController extends ApiController
{
    /** @var  ReservationRepository */
    protected $repository;

    public function __construct(ReservationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->paginate();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        return ['result' => $this->repository->delete($id)];
    }

}
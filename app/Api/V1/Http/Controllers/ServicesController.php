<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.09.15
 * Time: 18:22
 */
namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\CreateServiceRequest;
use App\Api\V1\Http\Requests\GetServicesRequest;
use App\Contracts\Repositories\ServicesRepository;
use App\Services\ApplicationService;
use App\Services\ServiceCreaterService;
use Illuminate\Http\Request;


/**
 * Class ServicesController
 * @Resource("Services", uri="/services")
 * @package App\Api\V1\Http\Controllers
 */
class ServicesController extends ApiController
{
    protected $repository;

    protected $creator;

    protected $applicationService;

    /**
     * @param ServicesRepository $repository
     * @param ServiceCreaterService $createrService
     * @param ApplicationService $applicationService
     */
    public function __construct(
        ServicesRepository $repository,
        ServiceCreaterService $createrService,
        ApplicationService $applicationService
    )
    {
        $this->middleware('auth.vendor', ['only' => ['store']]);
        $this->middleware('owner.salon');
        $this->repository = $repository;
        $this->creator = $createrService;
        $this->applicationService = $applicationService;
    }

    /**
     * @Get("/")
     * @Version({"v1"})
     * @Parameters({
     *      @Parameter("limit", description="The page result to view")
     * })
     * @Transaction({
     *       @Request({"limit": 5, "orderBy": "id", "sortedBy": "desc", "filter": "id;name", "search": "id:1"})
     * })
     * @param GetServicesRequest $request
     * @return mixed
     */
    public function index(GetServicesRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     * @Post("/")
     * @Version({"v1"})
     * @Transaction({
     *      @Request({"name": "foo", "duration": "111111", "price": "10000"}),
     *      @Response(200, body={"id":10, "name": "foo", "duration": "111111", "price": "10000", "updated_at": "2015-01-01 00:00:00"}),
     *      @Response(422, body={"message": "Fail", "errors": "array"})
     * })
     * @param  CreateServiceRequest  $request
     * @return Response
     */
    public function store(CreateServiceRequest $request)
    {
        return $this->creator->create($request->all(), $this->applicationService->getCurrentSalon());
    }

    /**
     * Display the specified resource.
     * @Get("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     * @Put("/:id")
     * @Version({"v1"})
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     * @Delete("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}
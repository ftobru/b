<?php

namespace App\Api\V1\Http\Controllers;


use App\Api\V1\Http\Requests\GetUsersRequest;
use App\Contracts\Repositories\UserRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends ApiController
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @param GetUsersRequest $request
     * @return Response
     */
    public function index(GetUsersRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}

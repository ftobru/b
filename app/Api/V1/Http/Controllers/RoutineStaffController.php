<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 29.10.15
 * Time: 22:56
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\CreateRoutineStaffRequest;
use App\Api\V1\Http\Requests\UpdateRoutineStaffRequest;
use App\Exceptions\RoutineExistException;
use App\Exceptions\SalonNotFoundException;
use App\Services\ApplicationService;


use App\Contracts\Repositories\RoutineStaffRepository;
use Illuminate\Support\Collection;

class RoutineStaffController extends ApiController
{
    /** @var RoutineStaffRepository  */
    protected $repository;

    /** @var ApplicationService  */
    protected $applicationService;

    /**
     * @param RoutineStaffRepository $repository
     * @param ApplicationService $applicationService
     */
    public function __construct(RoutineStaffRepository $repository, ApplicationService $applicationService)
    {
        $this->middleware('owner.salon');
        $this->repository = $repository;
        $this->applicationService = $applicationService;

    }

    public function index()
    {
        return $this->repository->paginate();
    }

    /**
     * @param CreateRoutineStaffRequest $request
     * @return mixed
     * @throws RoutineExistException
     * @throws SalonNotFoundException
     */
    public function store(CreateRoutineStaffRequest $request)
    {
        if(!($salonId = $this->applicationService->getCurrentSalon())) {
            throw new SalonNotFoundException;
        }
        $data = $request->all();
        $data = array_merge($data, ['salon_id' => $salonId]);
        /** @var Collection $routineStaff */
        $routineStaff = $this->repository->findWhere([
            'staff_id' => $request->get('staff_id'),
            'weekday' => $request->get('weekday')
        ]);

        \Log::debug('Store routine staff ------>' . print_r($routineStaff, true));

        if(!$routineStaff->isEmpty()) {
            throw new RoutineExistException;
        }
        return $this->repository->create($data);
    }

    /**
     * @param UpdateRoutineStaffRequest $request
     * @param $id
     * @return mixed
     */
    public function update(UpdateRoutineStaffRequest $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }
}
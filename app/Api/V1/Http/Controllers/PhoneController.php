<?php

namespace App\Api\V1\Http\Controllers;




use App\Api\V1\Http\Requests\GetPhonesRequest;
use App\Contracts\Repositories\PhoneRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class PhoneController extends ApiController
{
    protected $repository;

    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->middleware('auth.vendor', ['only' => ['store']]);
        $this->middleware('owner.salon');
        $this->repository = $phoneRepository;
    }

    /**
     * Display a listing of the resource.
     * @param GetPhonesRequest $request
     * @return Response
     */
    public function index(GetPhonesRequest $request)
    {
        return $this->repository->paginate($request->get('all'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}

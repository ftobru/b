<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.10.15
 * Time: 18:50
 */

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\GetRoutinesRequest;
use App\Contracts\Repositories\RoutineRepository;
use App\Services\ApplicationService;
use App\Services\RoutineCreaterService;
use Illuminate\Http\Request;


/**
 * Class RoutineController
 * @package App\Api\V1\Http\Controllers
 */
class RoutineController extends ApiController
{
    /** @var RoutineRepository  */
    protected $repository;
    /** @var RoutineCreaterService  */
    protected $createrService;
    /** @var ApplicationService  */
    protected $applicationService;

    public function __construct(
        RoutineRepository $repository,
        RoutineCreaterService $createrService,
        ApplicationService $applicationService
    )
    {
        $this->repository = $repository;
        $this->createrService = $createrService;
        $this->applicationService = $applicationService;
    }

    /**
     * @param GetRoutinesRequest $request
     * @return mixed
     */
    public function index(GetRoutinesRequest $request)
    {
        return $this->repository->paginate($request->all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\SalonNotFoundException
     */
    public function store(Request $request)
    {
        return $this->createrService->create($request->all(), $this->applicationService->getCurrentSalon());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}
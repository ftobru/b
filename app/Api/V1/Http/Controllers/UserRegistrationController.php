<?php
namespace App\Api\V1\Http\Controllers;

use App\Services\UserRegistrationService;
use Request;


/**
 * @Resource("User Registration", uri="/user/registration")
 * Class UserRegistrationController
 * @package App\Api\V1\Http\Controllers
 */
class UserRegistrationController extends ApiController
{
    /**
     * @var UserRegistrationService
     */
    protected $service;

    /**
     * @param UserRegistrationService $service
     */
    public function __construct(UserRegistrationService $service)
    {
        $this->service = $service;
    }

    /**
     * User registration
     * @Post("/")
     * @Version({"v1"})
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->service->registration($request->all());
    }

}
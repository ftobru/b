<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\GetAdminsRequest;
use App\Contracts\Repositories\AdminRepository;
use Illuminate\Http\Request;
use Dingo\Blueprint\Annotation\Response;
use App\Http\Requests;


/**
 * Class AdminController
 * @Resource("Admins", uri="/admins")
 * @package App\Api\V1\Http\Controllers
 */
class AdminController extends ApiController
{
    /**
     * @var AdminRepository
     */
    protected $repository;

    /**
     * @param AdminRepository $adminRepository
     */
    public function __construct(AdminRepository $adminRepository)
    {
        $this->repository = $adminRepository;
    }

    /**
     * Display a listing of the resource.
     * @Get("/")
     * @Version({"v1"})
     * @Parameters({
     *      @Parameter("limit", description="The page result to view")
     * })
     * @Transaction({
     *       @Request({"limit": 5, "orderBy": "id", "sortedBy": "desc", "filter": "id;name", "search": "id:1"})
     * })
     * @param GetAdminsRequest $request
     * @return Response
     */
    public function index(GetAdminsRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }


    /**
     * Store a newly created resource in storage.
     * @Post("/")
     * @Version({"v1"})
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     * @Get("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     * @Put("/:id")
     * @Version({"v1"})
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     * @Delete("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return ['result' => $this->repository->delete($id)];
    }
}

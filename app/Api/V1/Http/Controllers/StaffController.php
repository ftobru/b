<?php


namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Requests\GetStaffsRequest;
use App\Contracts\Repositories\StaffRepository;
use App\Services\ApplicationService;
use App\Services\StaffCreaterService;
use App\Validators\StaffValidator;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Http\Response;
use Illuminate\Http\Request;
use Log;
use Prettus\Validator\Contracts\ValidatorInterface;

/**
 * @Resource("Staffs", uri="/staffs")
 * Class StaffController
 * @package App\Api\V1\Http\Controllers
 */
class StaffController extends ApiController
{
    /**
     * @var StaffRepository
     */
    protected $repository;
    /**
     * @var StaffValidator
     */
    protected $validator;

    /** @var  StaffCreaterService */
    protected $creater;

    /** @var  ApplicationService */
    protected $applicationService;

    /**
     * @param StaffRepository $repository
     * @param StaffValidator $validator
     * @param StaffCreaterService $createrService
     * @param ApplicationService $applicationService
     */
    public function __construct(
        StaffRepository $repository,
        StaffValidator $validator,
        StaffCreaterService $createrService,
        ApplicationService $applicationService
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->applicationService = $applicationService;
        $this->creater = $createrService;
        $this->middleware('auth.vendor', ['only' => ['store']]);
        $this->middleware('owner.salon');
    }

    /**
     * @Get("/")
     * @Version({"v1"})
     * @Parameters({
     *      @Parameter("limit", description="The page result to view")
     * })
     * @param GetStaffsRequest $request
     * @return mixed
     */
    public function index(GetStaffsRequest $request)
    {
        return $this->repository->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     * @Post("/")
     * @Version({"v1"})
     * @Transaction({
     *      @Request({"title": "foo", "name": "bar"}),
     *      @Response(200, body={"title": "foo", "name": "bar", "created_at"}),
     *      @Response(422, body={"message": "Fail", "errors": "array"})
     * })
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if(!$this->validator->with($request->all())->passes(ValidatorInterface::RULE_CREATE)) {
            throw new StoreResourceFailedException('Fail create', $this->validator->errors());
        }

        $salonId = $this->applicationService->getCurrentSalon();
        $staffs  = $this->creater->create($request->all(), $salonId);
        return $staffs;
    }

    /**
     * Display the specified resource.
     * @Get("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }


    /**
     * Update the specified resource in storage.
     * @Put("/:id")
     * @Version({"v1"})
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->repository->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     * @Delete("/:id")
     * @Version({"v1"})
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return  ['result' => $this->repository->delete($id)];
    }
}
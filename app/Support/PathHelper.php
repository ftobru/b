<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 06.10.15
 * Time: 21:15
 */

namespace App\Support;

class PathHelper
{
    /**
     * @param $path
     * @param string $to
     * @return string
     */
    public static function cut($path, $to = 'public')
    {
        return substr($path, strpos($path, $to));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 13:28
 */

namespace App\ObjectValues;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class PaymentObject implements Arrayable, Jsonable
{
    /** @var int  */
    protected $userId;
    /** @var float  */
    protected $amount;
    /** @var  int */
    protected $promoId;
    /** @var string  */
    protected $paymentType;
    /** @var  int */
    protected $serviceId;
    /** @var  int */
    protected $salonId;

    protected $transactionId;

    protected $responseCode;

    protected $responseText;

    protected $deep;

    /**
     * @param $userId
     * @param $amount
     * @param $paymentType
     * @param $serviceId
     * @param $salonId
     * @param null $promoId|
     */
    public function __construct($userId, $amount, $paymentType, $serviceId, $salonId, $promoId = null)
    {
        $this->userId = (int)$userId;
        $this->amount = floatval($amount);
        $this->paymentType = (string)$paymentType;
        $this->serviceId = (int)$serviceId;
        $this->salonId = (int)$salonId;
        if($promoId) {
            $this->promoId = $promoId;
        }
    }



    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getPromoId()
    {
        return $this->promoId;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return int
     */
    public function getSalonId()
    {
        return $this->salonId;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * @param mixed $responseCode
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    }

    /**
     * @return mixed
     */
    public function getResponseText()
    {
        return $this->responseText;
    }

    /**
     * @param mixed $responseText
     */
    public function setResponseText($responseText)
    {
        $this->responseText = $responseText;
    }

    /**
     * @return mixed
     */
    public function getDeep()
    {
        return $this->deep;
    }

    /**
     * @param mixed $deep
     */
    public function setDeep(array $deep)
    {
        $this->deep = $deep;
    }



    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'user_id' => $this->getUserId(),
            'amount' => $this->getAmount(),
            'promo_id' => $this->getPromoId(),
            'payment_type' => $this->getPaymentType(),
            'service_id' => $this->getServiceId(),
            'salon_id' => $this->getSalonId(),
            'transaction_id' => $this->getTransactionId(),
            'response_code' => $this->getResponseCode(),
            'response_text' => $this->getResponseText(),
            'deep' => $this->getDeep()
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toJson(0);
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 21:14
 */

namespace App\ObjectValues;

/**
 * Class UserObject
 * @package App\ObjectValues
 */
class UserObject
{
    /** @var  string */
    protected $email;
    /** @var  string */
    protected $phone;
    /** @var  string */
    protected $fullName;
    /** @var  string */
    protected $name;

    /** @var string  */
    protected $surname;

    /**
     * @param $email
     * @param $phone
     * @param $fullName
     */
    public function __construct($email, $phone, $fullName)
    {
        $this->email = $email;
        $this->phone = $phone;
        $this->fullName = $fullName;
        $exp = explode(' ', $fullName);

        /**
         * Delimeter surname and name by full name
         */
        if(isset($exp[0])) {
            $this->name = $exp[0];
            unset($exp[0]);
            if(!empty($exp)) {
                $this->surname = implode(' ', $exp);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    public function getFullName()
    {
        return $this->fullName;
    }
}
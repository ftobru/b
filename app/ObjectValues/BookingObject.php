<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 15:18
 */

namespace App\ObjectValues;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class BookingObject implements Jsonable, Arrayable
{
    protected $salonId;

    protected $userId;

    protected $serviceId;

    protected $startTime;

    protected $endTime;

    protected $paymentId;

    protected $isDiscount;

    protected $promoId;

    protected $staffId;

    public function __construct($salonId, $userId, $staffId, $serviceId, $startTime, $paymentId = null, $promoId = null)
    {
        $this->salonId = (int)$salonId;

        $this->userId = (int)$userId;
        $this->staffId = (int)$staffId;
        $this->serviceId = (int)$serviceId;
        $this->startTime = (string)$startTime;
        $this->paymentId = (int)$paymentId;
    }



    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'salon_id' => $this->getSalonId(),
            'user_id' => $this->getUserId(),
            'service_id' => $this->getServiceId(),
            'start_time' => $this->getStartTime(),
            'end_time' => $this->getEndTime(),
            'payment_id' => $this->getPaymentId(),
            'is_discount' => $this->getIsDiscount(),
            'promo_id' => $this->getPromoId(),
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * @return mixed
     */
    public function getSalonId()
    {
        return $this->salonId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @return mixed
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @return mixed
     */
    public function getIsDiscount()
    {
        return $this->isDiscount;
    }

    /**
     * @return mixed
     */
    public function getPromoId()
    {
        return $this->promoId;
    }

    /**
     * @return mixed
     */
    public function getStaffId()
    {
        return $this->staffId;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @param mixed $isDiscount
     */
    public function setIsDiscount($isDiscount)
    {
        $this->isDiscount = $isDiscount;
    }


}

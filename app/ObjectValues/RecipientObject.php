<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 15:59
 */

namespace App\ObjectValues;

class RecipientObject
{

    protected $id;

    protected $to;

    protected $type;

    public function __construct($id, $to, $type)
    {
        $this->id = $id;
        $this->to = $to;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'to' => $this->getTo(),
            'type' => $this->getType()
        ];
    }


}
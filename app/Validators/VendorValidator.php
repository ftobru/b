<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:29
 */

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class VendorValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|min:6|max:255',
            'email' => 'required|email|min:6|max:255',
            'password' => 'required|min:6|max:255',
            'phone' => 'required|min:6|max:255'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|min:6|max:255',
            'email' => 'required|email|min:6|max:255',
            'password' => 'required|min:6|max:255',
            'phone' => 'required|min:6|max:255',
            'status' => 'required|status:SalonReference'
        ]
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:30
 */


namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class BrandValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|min:6|max:255|unique:brands',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|min:6|max:255|unique:brands',
        ]
    ];
}
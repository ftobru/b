<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 07.10.15
 * Time: 8:25
 */

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class StaffValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|min:1|max:255',
            'title' => 'required|min:1|max:255',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|min:1|max:255',
            'title' => 'required|min:1|max:255'
        ]
    ];
}

<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:29
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class VendorConfirmationCodeValidator extends LaravelValidator
{
    protected $rules = [
        'code' => 'exists:users'
    ];
}
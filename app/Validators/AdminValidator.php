<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:30
 */

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AdminValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|unique:admins|min:6|max:255',
            'email' => 'required|unique:admins|min:6|max:255',
            'phone' => 'required|unique:admins|min:6|max:255',
            'password' => 'required|min:6|max:255'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|unique:admins',
            'email' => 'required|unique:admins',
            'phone' => 'required|unique:admins',
            'password' => 'min:6|max:255',
            'status' => 'required'
        ]
    ];
}
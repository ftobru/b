<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:28
 */
namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class UserConfirmationCodeValidator extends LaravelValidator
{
    protected $rules = [
        'code' => 'exists:users'
    ];
}
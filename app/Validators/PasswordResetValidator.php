<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:32
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class PasswordResetValidator
 * @package App\Validators
 */
class PasswordResetValidator extends LaravelValidator
{
    protected $rules = [
            'email' => 'email|required|min:6|max:255',
            'token' => 'required|min:6|max:255'
    ];
}
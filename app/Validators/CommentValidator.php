<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:31
 */

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

/**
 * Class CommentValidator
 * @package App\Validators
 */
class CommentValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'message' => 'required|min:6|max:2048',
            'user_id' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'message' => 'required|min:6|max:2048',
            'status' => 'required|status:CommentReference'
        ]
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 17:00
 */
namespace App\Validators\Rules;


use App\References\Interfaces\StatusInterface;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Validation\Validator;

/**
 * Class CustomRule
 * @package App\Validators\Rules
 */
class CustomRule extends Validator
{
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     */
    public function validateStatus($attribute, $value, $parameters)
    {
        Collection::make($parameters)->each(function($val) use ($value) {
            $reference = app($val);
            if ($reference instanceof StatusInterface) {
                if (!in_array($value, $reference->statuses())) {
                    return false;
                }
            } else {
                throw new FileNotFoundException;
            }
        });
    }
}

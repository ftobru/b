<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:29
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class PhoneValidator
 * @package App\Validators
 */
class PhoneValidator extends LaravelValidator
{
    protected $rules = [
        'number' => 'required|min:6|max:255'
    ];
}
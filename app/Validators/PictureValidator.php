<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 11:10
 */

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class PictureValidator extends LaravelValidator
{
    protected $rules = [
        'picture' => 'image|size:10240|required',
    ];
}
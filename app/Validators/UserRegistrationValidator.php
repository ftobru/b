<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:26
 */


namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class UserRegistrationValidator extends LaravelValidator
{
    protected $rules = [
        'name' => 'required|min:6|max:255',
        'surname' => 'required|min:6|max:255',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6|max:255',
        'phone' => 'required|phone'
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:29
 */

namespace App\Validators;


use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

/**
 * Class SalonValidator
 * @package App\Validators
 */
class SalonValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|min:6|max:255|unique:salons',
            'address' => 'required|min:6|max:255',
            'lat' => 'required',
            'lng' => 'required',

        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|min:6|max:255',
            'address' => 'required|min:6|max:255',
            'lat' => 'required',
            'lng' => 'required',
            'status' => 'status:SalonReference'
        ],
    ];
}
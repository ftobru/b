<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 15:54
 */


namespace App\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class VendorRegistrationValidator
 * @package App\Validators
 */
class VendorRegistrationValidator extends LaravelValidator
{
    protected $rules = [
        'vendor.name' => 'required|min:3|max:255',
        'vendor.email' => 'required|email|min:6|max:255',
        'vendor.phone' => 'required|min:6|max:255',
        'salon.name' => 'required|min:3|max:255',
        'salon.address' => 'required|min:3|max:255',
        'salon.lat' => 'required',
        'salon.lng' => 'required',
        'salon.postcode' => 'required',
        'salon.type' => 'required',
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 14:07
 */

namespace App\Services;

use App\Collections\RecipientsCollection;
use App\Contracts\Repositories\SmsRepository;
use App\Contracts\Services\SmsTransportInterface;
use App\ObjectValues\RecipientObject;
use App\Repositories\Eloquent\SmsRepositoryEloquent;
use Clickatell\TransportInterface;
use Exception;
use Illuminate\Support\Collection;
use Log;


/**
 * Class SmsTransportService
 * @package App\Services
 */
class SmsTransportService implements SmsTransportInterface
{
    /**
     * @var TransportInterface
     */
    protected $gateService;

    /** @var  SmsRepository|SmsRepositoryEloquent */
    protected $smsRepository;

    public function __construct(TransportInterface $transportInterface, SmsRepository $repository)
    {
        $this->gateService = $transportInterface;
        $this->smsRepository = $repository;
    }

    /**
     * @param RecipientsCollection $recipientsCollections
     * @param $message
     * @param array $extra
     * @return RecipientsCollection
     */
    public function sendMessage(RecipientsCollection $recipientsCollections, $message, $extra = [])
    {

        $recipientsCollections->each(function($recipient) use ($message, $extra){
            /** @var RecipientObject  $recipient */
            $response = new Collection($this->gateService->sendMessage($recipient->getTo(), $message, $extra));
            $sms = [];
            $response->each(function($message) use ($sms, $recipient) {
                $sms = [
                    'api_id' => object_get($message, 'id', 0),
                    'destination' => object_get($message, 'destination', null),
                    'error' => object_get($message, 'error', null),
                    'errorCode' => object_get($message, 'errorCode', null),
                ];
                $sms = array_merge($sms, $recipient->toArray());
                try {
                    $this->smsRepository->create($sms);
                } catch(Exception $e) {
                    Log::error('Sms not save. Persistent panic!', $sms);
                }
            });
        });

        return $recipientsCollections;
    }


    /**
     * Get Balance (By Gate)
     * @return mixed
     */
    public function getBalance()
    {
        return $this->gateService->getBalance();
    }

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function stopMessage($apiMsgId)
    {
        return $this->gateService->stopMessage($apiMsgId);
    }

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function queryMessage($apiMsgId)
    {
        return $this->gateService->queryMessage($apiMsgId);
    }

    /**
     * @param $msisdn
     * @return mixed
     */
    public function routeCoverage($msisdn)
    {
        return $this->gateService->queryMessage($msisdn);
    }

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function getMessageCharge($apiMsgId)
    {
        return $this->gateService->queryMessage($apiMsgId);
    }


}
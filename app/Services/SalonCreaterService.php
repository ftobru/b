<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 20:50
 */

namespace App\Services;


use App\Contracts\Repositories\SalonRepository;
use Dingo\Api\Exception\StoreResourceFailedException;

class SalonCreaterService
{

    protected $salonRepository;

    protected $uploaderPicture;

    /**
     * @param SalonRepository $salonRepository
     * @param UploaderPictureService $uploaderPictureService
     */
    public function __construct(SalonRepository $salonRepository, UploaderPictureService $uploaderPictureService)
    {
        $this->salonRepository = $salonRepository;
        $this->uploaderPicture = $uploaderPictureService;
    }

    /**
     * @param $salonData
     * @param null $picture
     * @return mixed
     */
    public function create($salonData, $picture = null)
    {
        $salon = $this->salonRepository->create($salonData);
        if(!$salon) {
            throw new StoreResourceFailedException;
        }
        if($picture) {
            $this->uploaderPicture; // @todo upload pictures
        }
        return $salon;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 07.10.15
 * Time: 8:31
 */

namespace App\Services;

use App\Contracts\Repositories\StaffRepository;

class RegistrationStaffService
{
    protected $repository;

    public function __construct(StaffRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $staff
     * @return mixed
     */
    public function add($staff)
    {
        return $this->repository->create($staff);
    }


}
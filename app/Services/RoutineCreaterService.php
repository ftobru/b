<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 23.10.15
 * Time: 15:59
 */

namespace App\Services;

use App\Contracts\Repositories\RoutineRepository;
use App\Contracts\Repositories\SalonRepository;
use App\Entities\Salon;
use App\Exceptions\SalonNotFoundException;

class RoutineCreaterService
{
    /** @var RoutineRepository  */
    protected $routinesRepository;

    protected $salonRepository;

    public function __construct(RoutineRepository $repository, SalonRepository $salonRepository)
    {
        $this->routinesRepository = $repository;
        $this->salonRepository = $salonRepository;
    }

    /**
     * @param array $routingData
     * @param null $salonId
     * @return \Illuminate\Database\Eloquent\Model
     * @throws SalonNotFoundException
     */
    public function create(array $routingData, $salonId)
    {
        if($salon = $this->salonRepository->find($salonId)) {
            /** @var  Salon $salon */
            return $salon->routines()->save($this->routinesRepository->create($routingData));
        } else {
            throw new SalonNotFoundException;
        }
    }
}
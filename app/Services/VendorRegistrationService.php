<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 21.08.15
 * Time: 11:37
 */

namespace App\Services;


use App\Contracts\Repositories\SalonRepository;
use App\Contracts\Repositories\VendorRepository;
use App\Entities\Vendor;
use App\Repositories\Eloquent\SalonRepositoryEloquent;
use App\Repositories\Eloquent\VendorRepositoryEloquent;
use Dingo\Api\Exception\StoreResourceFailedException;

class VendorRegistrationService
{
    /** @var VendorRepository| VendorRepositoryEloquent */
    protected $vendorRepository;
    /** @var SalonRepository| SalonRepositoryEloquent */
    protected $salonRepository;

    /**
     * @param VendorRepository $vendorRepository
     * @param SalonRepository $salonRepository
     */
    public function __construct(VendorRepository $vendorRepository, SalonRepository $salonRepository) {
        $this->vendorRepository = $vendorRepository;
        $this->salonRepository  = $salonRepository;
    }

    /**
     * @param array $vendorData
     * @return Vendor
     */
    public function registration(array $vendorData) {

        $vendor = $this->vendorRepository->create($vendorData['vendor']);
        if (!$vendor) {
            throw new StoreResourceFailedException('Vendor not create');
        }

        $this->salonRepository->create(array_add($vendorData['salon'], 'vendor_id', $vendor->id));
        return $vendor;
    }
}
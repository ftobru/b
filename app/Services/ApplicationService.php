<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 14:24
 */

namespace App\Services;

use App\Contracts\Repositories\SalonRepository;
use App\Entities\Salon;
use App\Entities\Vendor;
use App\Exceptions\SalonNotFoundException;
use Auth;
use Illuminate\Session\Store;
use Ollieread\Multiauth\Guard;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ApplicationService
{
    /** @var  Guard */
    protected $vendorAuth;

    /** @var  Guard */
    protected $userAuth;

    /** @var  Guard */
    protected $adminAuth;

    /** @var Store  */
    protected $store;

    protected $salonRepository;

    /**
     * @param Store $manager
     * @param SalonRepository $repository
     */
    public function __construct(Store $manager, SalonRepository $repository)
    {
        $this->vendorAuth = Auth::vendor();
        $this->userAuth = Auth::user();
        $this->adminAuth = Auth::admin();
        $this->salonRepository = $repository;
        $this->store = $manager;
    }

    /**
     * @return int
     * @throws SalonNotFoundException
     */
    public function getCurrentSalon()
    {

        if(!($vendor = $this->vendorAuth->get())) {
            throw new AccessDeniedHttpException;
        }
        if($salon = $this->store->get('salon_id')) {
            $salon = $this->salonRepository->find($salon);
        } else {
            /** @var Vendor $vendor */
            $salon = $vendor->salons()->get()->shift();

        }
        if(!$salon) {
            throw new SalonNotFoundException;
        }
        return $salon->id;
    }
}
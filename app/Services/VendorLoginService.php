<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:19
 */

namespace App\Services;


use App\Contracts\Repositories\VendorRepository;
use Auth;
use Illuminate\Auth\Guard;

class VendorLoginService
{
    protected $repository;
    protected $auth;

    /**
     * @param VendorRepository $repository
     */
    public function __construct(VendorRepository $repository)
    {
        $this->repository = $repository;
        $this->auth = Auth::vendor();
    }

    /**
     * @param $phone
     * @param $password
     * @return bool
     */
    public function login($phone, $password)
    {
        return $this->auth->attempt(['phone' => $phone, 'password' => $password]);
    }


}
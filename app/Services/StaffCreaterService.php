<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 14:21
 */

namespace App\Services;


use App\Contracts\Repositories\SalonRepository;
use App\Contracts\Repositories\StaffRepository;
use App\Entities\Salon;
use App\Exceptions\SalonNotFoundException;
use Log;

class StaffCreaterService
{
    /** @var StaffRepository  */
    protected $staffRepository;
    /** @var SalonRepository  */
    protected $salonRepository;

    /**
     * @param StaffRepository $repository
     * @param SalonRepository $salonRepository
     */
    public function __construct(StaffRepository $repository, SalonRepository $salonRepository)
    {
        $this->staffRepository = $repository;
        $this->salonRepository = $salonRepository;
    }

    /**
     * @param array $staffData
     * @param $salonId
     * @return \Illuminate\Database\Eloquent\Model
     * @throws SalonNotFoundException
     */
    public function create(array $staffData, $salonId)
    {
        if(!($salon = $this->salonRepository->find($salonId))){
            throw new SalonNotFoundException;
        }
        /** @var Salon $salon */
        $staffData['salon_id'] = $salon->id;

        Log::debug($staffData);
        $staff = $this->staffRepository->create($staffData);
        return $staff;
    }
}
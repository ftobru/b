<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 27.10.15
 * Time: 11:15
 */

namespace App\Services;

use App\Contracts\Repositories\SalonRepository;
use App\Support\PathHelper;
use App\Validators\PictureValidator;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AddPictureService
{
    /** @var SalonRepository  */
    protected $repository;

    /** @var PictureValidator  */
    protected $validator;

    /**
     * @param SalonRepository $repository
     * @param PictureValidator $validator
     */
    public function __construct(SalonRepository $repository, PictureValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param array $pictures
     */
    public function add(array $pictures)
    {
        $picturesCollection = new Collection($pictures);
        $picturesCollection->each(function($picture){
            /** @var UploadedFile $picture */
            if(!$this->validator->with(['picture' => $picture->getPath()])->passes()) {
                throw new UploadException;
            }
            $filename = public_path('image') . '/' . $picture->getClientOriginalName();
            $picture->move(public_path('image') . '/');
        });
    }


}
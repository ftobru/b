<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 21:08
 */

namespace App\Services;

use App\Contracts\Repositories\UserRepository;
use App\Entities\User;
use App\Exceptions\UserAutoRegistrationException;
use App\ObjectValues\UserObject;
use App\References\MailReference;
use Log;
use Mail;


/**
 * Class AutoRegistrationUserService
 * @package App\Services
 */
class AutoRegistrationUserService
{
    /** @var UserRepository  */
    protected $userRepository;

    /** @var Mail  */
    protected $mail;

    /**
     * @param UserRepository $userRepository
     * @param Mail $mail
     */
    public function __construct(UserRepository $userRepository, Mail $mail)
    {
        $this->userRepository = $userRepository;
        $this->mail = $mail;
    }

    /**
     * @param UserObject $userObject
     * @return User
     * @throws UserAutoRegistrationException
     */
    public function registration(UserObject $userObject)
    {
        $userData = [
            'email' => $userObject->getEmail(),
            'phone' => $userObject->getPhone(),
            'name' => $userObject->getName(),
            'surname' => $userObject->getSurname(),
            'password' => str_random(6)
        ];
        try {
            $user = $this->userRepository->create($userData);

            $this->mail->send('mail.user.registration', $userData, function($message) use ($userObject) {
                $message->from(MailReference::SUPPORT_BOX);
                $message->to($userObject->getEmail());
            });

        } catch(\Exception $e) {
            Log::emergency('Fatal error. User registration fail', $userData);
            throw new UserAutoRegistrationException;
        }

        return $user;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 21.08.15
 * Time: 11:37
 */

namespace App\Services;


use App\Contracts\Repositories\UserRepository;
use App\Entities\Vendor;
use App\Repositories\Eloquent\UserRepositoryEloquent;

class UserRegistrationService
{
    /** @var UserRepository| UserRepositoryEloquent */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $userData
     * @return Vendor
     */
    public function registration(array $userData) {
        $user = $this->userRepository->create($userData);
        return $user;
    }
}
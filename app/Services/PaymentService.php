<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 28.10.15
 * Time: 13:59
 */

namespace App\Services;


use App\Contracts\Repositories\PaymentsRepository;
use App\Contracts\Repositories\UserRepository;
use App\Exceptions\UserNotFoundException;
use App\ObjectValues\PaymentObject;
use Braintree\ClientToken;
use Braintree\Configuration;
use Braintree\Transaction;
use Config;

class PaymentService
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var PaymentsRepository
     */
    protected $paymentRepository;

    /**
     * @param UserRepository $userRepository
     * @param PaymentsRepository $paymentsRepository
     */
    public function __construct(UserRepository $userRepository, PaymentsRepository $paymentsRepository)
    {
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentsRepository;
        Configuration::environment(Config::get('payment.environment'));
        Configuration::merchantId(Config::get('payment.merchantId'));
        Configuration::publicKey(Config::get('payment.publicKey'));
        Configuration::privateKey(Config::get('payment.privateKey'));
    }

    /**
     * @return array
     */
    public function generateToken()
    {
        return ClientToken::generate();
    }

    /**
     * @param PaymentObject $paymentObject
     * @return array
     * @throws UserNotFoundException
     */
    public function payment(PaymentObject $paymentObject)
    {
        if(!($user = $this->userRepository->find($paymentObject->getUserId()))) {
            throw new UserNotFoundException;
        }

        /** @var object $result */
        $result = Transaction::sale([
            'amount' => $paymentObject->getAmount(),
            'paymentMethodNonce' => $paymentObject->getPaymentType(),
            'options' => [ 'submitForSettlement' => true ]
        ]);

        if ($result->success) {
            $paymentObject->setTransactionId($result->transaction->id);
        } else if ($result->transaction) {
            $paymentObject->setResponseCode($result->transaction->processorResponseCode);
            $paymentObject->setResponseText($result->transaction->processorResponseText);
        } else {
            $paymentObject->setDeep($result->errors->deepAll());
        }

        return $this->paymentRepository->create($paymentObject->toArray());
    }

    public function paymentCash(PaymentObject $paymentObject)
    {
        return $this->paymentRepository->create($paymentObject->toArray());
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 18:26
 */

namespace App\Services;

use App\Contracts\Repositories\PictureRepository;
use App\Contracts\Repositories\SalonRepository;
use App\Contracts\Repositories\VendorRepository;
use App\Entities\Vendor;
use App\Exceptions\NotCreateVendorException;
use App\Exceptions\SalonCanNotCreateException;
use App\Exceptions\UploadPictureException;
use App\Repositories\Eloquent\PictureRepositoryEloquent;
use App\Support\PathHelper;
use Exception;
use Illuminate\Support\Str;
use Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class VendorCreaterService
 * @package App\Services
 */
class VendorCreaterService
{
    /** @var VendorRepository  */
    protected $vendorRepository;
    /** @var SalonRepository  */
    protected $salonRepository;
    /** @var  PictureRepository|PictureRepositoryEloquent */
    protected $pictureRepository;

    /**
     * @param VendorRepository $repository
     * @param SalonRepository $salonRepository
     * @param PictureRepository $pictureRepository
     */
    public function __construct(
        VendorRepository $repository,
        SalonRepository $salonRepository,
        PictureRepository $pictureRepository
    )
    {
        $this->vendorRepository = $repository;
        $this->salonRepository = $salonRepository;
        $this->pictureRepository = $pictureRepository;
    }

    /**
     * @param array $vendorData
     * @param array $salonData
     * @return Vendor
     * @throws NotCreateVendorException
     */
    public function create(array $vendorData, array $salonData = [])
    {
        /** @var Vendor $vendor */
        $vendor = $this->vendorRepository->create($vendorData);
        if($vendor) {
            if(!empty($salonData)) {
                $vendor->salons()->save($this->salonRepository->create($salonData));
            }
        } else {
            throw new NotCreateVendorException;
        }
        return $vendor;
    }

    public function createWithPicture(array $vendorData, array $salonData, array $pictures)
    {
        try {
            /** @var Vendor $vendor */
            $vendor = $this->vendorRepository->create($vendorData);
        } catch(Exception $e) {
            throw new NotCreateVendorException;
        }

        try {
            $salonData = array_merge($salonData, ['vendor_id' => $vendor->id]);
            $salon = $this->salonRepository->create($salonData);
        } catch(Exception $e) {
            throw new SalonCanNotCreateException($e->getMessage());
        }

        try {
            $pictureData = [
                'salon_id' => $salon->id
            ];
            foreach($pictures as $key => $picture) {
                /** @var UploadedFile $picture */
                //dd($picture->getPath() . '/' .  $picture->getBasename());
                $image = Image::make($picture->getPath() . '/' .  $picture->getBasename());
                $pathSave = public_path('images/salons/' . $vendor->id . '_' . $picture->getBasename().  '.' . $picture->getClientOriginalExtension());
                if($key === 'salon_picture_580_340') {
                    $image->resize(580, 340)->save($pathSave);
                    $pictureData = array_merge($pictureData, ['pictures' => ['580х340' => PathHelper::cut($pathSave)]]);
                } else if($key === 'salon_picture_255_155') {
                    $image->resize(255, 155)->save($pathSave);
                    $pictureData = array_merge($pictureData, ['pictures' => ['255х155' => PathHelper::cut($pathSave)]]);
                }

            }
            $this->pictureRepository->create($pictureData);

        } catch(Exception $e) {
            throw new UploadPictureException($e->getMessage());
        }

        return $vendor;
    }

}
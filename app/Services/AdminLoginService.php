<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:25
 */

namespace App\Services;


use App\Contracts\Repositories\AdminRepository;
use Auth;
use Ollieread\Multiauth\Guard;

/**
 * Class AdminLoginService
 * @package App\Services
 */
class AdminLoginService
{
    protected $repository;

    /** @var Guard auth */
    protected $auth;

    /**
     * @param AdminRepository $repository
     */
    public function __construct(AdminRepository $repository)
    {
        $this->repository = $repository;

        $this->auth = Auth::admin();
    }

    /**
     * @param $email
     * @param $password
     * @param bool $rememberMe
     * @return bool
     */
    public function login($email, $password, $rememberMe = false)
    {
        return $this->auth->attempt(['email' => $email, 'password' => $password], $rememberMe);

    }
}
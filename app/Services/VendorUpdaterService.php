<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 17:59
 */

namespace App\Services;

use App\Contracts\Repositories\VendorRepository;
use App\Entities\Salon;
use App\Entities\Vendor;
use App\Exceptions\SalonNotFoundException;
use App\Exceptions\VendorNotFoundException;


/**
 * Class VendorUpdaterService
 * @package App\Services
 */
class VendorUpdaterService
{
    /** @var VendorRepository  */
    protected $vendorRepository;

    /**
     * @param VendorRepository $repository
     */
    public function __construct(VendorRepository $repository)
    {
        $this->vendorRepository = $repository;
    }

    /**
     * Vendor update information
     * @param $id
     * @param $vendorData
     * @param array $salonData
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws SalonNotFoundException
     * @throws VendorNotFoundException
     */
    public function update($id, array $vendorData, array $salonData = [])
    {
        /** @var Vendor $vendor */
        $vendor = $this->vendorRepository->update($vendorData, $id);
        if($vendor) {
            /** @var Salon $salon */
            $salon = $vendor->salons()->get()->shift();
            if($salon) {
                if(!empty($salonData)) {
                    $salon->update($salonData);
                }
            } else {
                throw new SalonNotFoundException;
            }
        } else {
            throw new VendorNotFoundException;
        }

        return $vendor->with('salons')->get();
    }
}
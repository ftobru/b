<?php

namespace App\Services;


use App\Contracts\Repositories\SalonRepository;
use App\Contracts\Repositories\ServicesRepository;
use App\Entities\Salon;
use App\Exceptions\SalonNotFoundException;


/**
 * Class ServiceCreaterService
 * @package App\Services
 */
class ServiceCreaterService
{
    /** @var ServicesRepository  */
    protected $serviceRepository;

    /** @var SalonRepository  */
    protected $salonRepository;

    public function __construct(ServicesRepository $repository, SalonRepository $salonRepository)
    {
        $this->serviceRepository = $repository;
        $this->salonRepository = $salonRepository;
    }

    /**
     * @param array $serviceData
     * @param $salonId
     * @return \Illuminate\Database\Eloquent\Model
     * @throws SalonNotFoundException
     */
    public function create(array $serviceData, $salonId)
    {
        if($salon = $this->salonRepository->find($salonId)) {
            /** @var  Salon $salon */
            $serviceData['salon_id'] = $salonId;
            return $this->serviceRepository->create($serviceData);
        } else {
            throw new SalonNotFoundException;
        }
    }
}
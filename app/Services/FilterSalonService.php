<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 12.10.15
 * Time: 18:29
 */

namespace App\Services;

use App\Contracts\Repositories\SalonRepository;
use App\Repositories\Eloquent\Criteria\AnyDurationCriteria;
use App\Repositories\Eloquent\Criteria\AreaCriteria;
use App\Repositories\Eloquent\Criteria\AvailibilityCriteria;
use App\Repositories\Eloquent\Criteria\BrandsFilterCriteria;
use App\Repositories\Eloquent\Criteria\DayCriteria;
use App\Repositories\Eloquent\Criteria\OrderPriceHigherCriteria;
use App\Repositories\Eloquent\Criteria\OrderPriceLowerCriteria;
use App\Repositories\Eloquent\Criteria\PriceRangeCriteria;
use App\Repositories\Eloquent\Criteria\RatingDescCriteria;
use App\Repositories\Eloquent\Criteria\ServicesCriteria;
use App\Repositories\Eloquent\Criteria\ServiceTypeCriteria;
use App\Repositories\Eloquent\Criteria\ServiceTypesCriteria;
use App\Repositories\Eloquent\SalonRepositoryEloquent;
use Carbon\Carbon;
use DB;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class FilterSalonService
{
    /** @var SalonRepository|SalonRepositoryEloquent */
    protected $repository;

    protected $availibilityDate;
    /** @var  Carbon */
    protected $availibilityTimeStart;
    /** @var  Carbon */
    protected $availibilityTimeEnd;

    protected $location;

    protected $locationRadius;

    protected $serviceTypes;

    protected $services;

    protected $priceRangeMin;

    protected $priceRangeMax;

    protected $brands;

    protected $salons;

    /**
     * @return SalonRepository|SalonRepositoryEloquent
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return mixed
     */
    public function getAvailibilityDate()
    {
        return $this->availibilityDate;
    }

    /**
     * @return mixed
     */
    public function getAvailibilityTimeStart()
    {
        return $this->availibilityTimeStart;
    }

    /**
     * @return mixed
     */
    public function getAvailibilityTimeEnd()
    {
        return $this->availibilityTimeEnd;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return mixed
     */
    public function getLocationRadius()
    {
        return $this->locationRadius;
    }

    /**
     * @return mixed
     */
    public function getServiceTypes()
    {
        return $this->serviceTypes;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getPriceRangeMin()
    {
        return $this->priceRangeMin;
    }

    /**
     * @return mixed
     */
    public function getPriceRangeMax()
    {
        return $this->priceRangeMax;
    }

    /**
     * @return mixed
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * @return mixed
     */
    public function getSalons()
    {
        return $this->salons;
    }


    /**
     * @param SalonRepository|SalonRepositoryEloquent $repository
     */
    public function __construct(SalonRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param null $availibilityDate
     * @param null $availibilityTimeStart
     * @param null $availibilityTimeEnd
     * @return $this
     */
    public function setAvailibility(
        $availibilityDate = null,
        $availibilityTimeStart = null,
        $availibilityTimeEnd = null
    )
    {
        if(trim($availibilityDate) && trim($availibilityTimeStart) && trim($availibilityTimeEnd)) {
            $date = new Carbon(strtotime($availibilityDate));
            $timeStartSeconds = $this->convertTimeToSeconds($availibilityTimeStart);
            $startTime = $date->addSeconds($timeStartSeconds);
            $timeEndSeconds = $this->convertTimeToSeconds($availibilityTimeEnd);
            $endTime = $date->addSeconds($timeEndSeconds);

            $this->repository->pushCriteria(new AvailibilityCriteria($startTime, $endTime));
            $this->availibilityTimeStart = $startTime;
            $this->availibilityTimeEnd = $endTime;
        }

        return $this;
    }


    /**
     * @param $time
     * @return mixed
     */
    private function convertTimeToSeconds($time)
    {
        /**
         * @var $minutes
         * @var $seconds
         */
        sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);

        return isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
    }

    public function setLocation($location = null)
    {
        //@todo location
    }

    /**
     * @param array $types
     * @return $this
     */
    public function setServiceTypes(array $types = [])
    {
        if(!empty($types)) {
            $this->repository->pushCriteria(new ServiceTypesCriteria($types));
            $this->serviceTypes = $types;
        }

        return $this;
    }

    /**
     * @param array $services
     * @return $this
     */
    public function setServices(array $services = [])
    {
        if(!empty($services)) {
            $this->repository->pushCriteria(new ServicesCriteria($services));
            $this->services = $services;
        }

        return $this;
    }

    /**
     * @param null $priceRangeMin
     * @param null $priceRangeMax
     * @return $this
     */
    public function setPriceRange($priceRangeMin = null, $priceRangeMax = null)
    {
        if($priceRangeMin && $priceRangeMax) {
            $this->repository->pushCriteria(new PriceRangeCriteria($priceRangeMin, $priceRangeMin));
            $this->priceRangeMin = $priceRangeMin;
            $this->priceRangeMax = $priceRangeMax;
        }

        return $this;
    }

    /**
     * @param array $brands
     * @return $this
     */
    public function setBrands(array $brands = [])
    {
        if(!empty($brands)) {
            $this->repository->pushCriteria(new BrandsFilterCriteria($brands));
            $this->brands = $brands;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function sortByRating()
    {
        $this->repository->pushCriteria(new RatingDescCriteria());
        return $this;
    }

    /**
     * @return $this
     */
    public function sortByPriceLower()
    {
        $this->repository->pushCriteria(new OrderPriceLowerCriteria());
        return $this;
    }

    /**
     * @return $this
     */
    public function sortByPriceUpper()
    {
        $this->repository->pushCriteria(new OrderPriceHigherCriteria());
        return $this;
    }

    /**
     * @param null $limit
     * @return mixed
     */
    public function all($limit = null)
    {

        /** @var LengthAwarePaginator $result */
        $result = $this->repository->paginate($limit);

        $builder = $this->filterByCriteria();
        $filterData = [
            'priceRange' => $this->getPriceRange(clone $builder),
            'serviceTypes' => $this->getSalonsServiceTypes(clone $builder),
            'services' => $this->getSalonsService(clone $builder),
            'brands' => $this->getSalonsBrands(clone $builder)
        ];


        return ['salons' => $result, 'filterData' => $filterData];
    }

    protected function filterByCriteria()
    {
        /** @var \Illuminate\Database\Query\Builder $builder */
        $builder = clone $this->repository->makeModel()->query();
        if($this->priceRangeMin && $this->priceRangeMax) {
            $builder->leftJoin('services as s', 'salons.id', '=', 's.salon_id')
                ->whereBetween('s.price', [$this->priceRangeMin, $this->priceRangeMax]);
        }

        if(is_array($this->brands) && !empty($this->brands)) {
            $builder->leftJoin('brand_salon as bs', 'salons.id', '=', 'bs.salon_id')
                ->leftJoin('brands as b', 'b.id', '=', 'bs.brand_id')
                ->whereIn('b.id', $this->brands);
        }

        if(is_array($this->serviceTypes) && !empty($this->serviceTypes)) {
            $builder
                ->whereIn('s.service_type_id', $this->serviceTypes);
        }

        if(is_array($this->services) && !empty($this->services)) {
            $builder
                ->whereIn('s.id', $this->services);
        }

        if(trim($this->availibilityTimeStart) && trim($this->availibilityTimeEnd)) {
            $builder->leftJoin('bookings', 'bookings.salon_id', '=', 'salons.id');
            $builder->whereNotBetween('bookings.start_time', [
                $this->availibilityTimeStart->toDateTimeString(),
                $this->availibilityTimeEnd->toDateTimeString()
            ])->whereNotBetween('bookings.end_time', [
                $this->availibilityTimeStart->toDateTimeString(),
                $this->availibilityTimeEnd->toDateTimeString()
            ]);
        }

        return $builder;
    }


    protected function getPriceRange(Builder $builder)
    {
        return $builder->getQuery()
            ->leftJoin('services', 'services.salon_id', '=', 'salons.id')
            ->select([DB::raw('MAX(services.price)'), DB::raw('MIN(services.price)')])->get();
    }

    protected function getSalonsServiceTypes(Builder $builder)
    {
        return $builder->getQuery()
            ->leftJoin('services', 'services.salon_id', '=', 'salons.id')
            ->select([DB::raw('DISTINCT("services"."service_type_id")')])->get();
    }

    protected function getSalonsService(Builder $builder)
    {
        return $builder->getQuery()
            ->leftJoin('services', 'services.salon_id', '=', 'salons.id')
            ->select([DB::raw('DISTINCT(services.name)')])->get();
    }



    protected function getSalonsBrands(Builder $builder)
    {
        return $builder->getQuery()->leftJoin('brand_salon', 'salons.id', '=', 'brand_salon.salon_id')
            ->leftJoin('brands', 'brands.id', '=', 'brand_salon.brand_id')
            ->select([DB::raw('DISTINCT(brands.name)')])->get();
    }





}
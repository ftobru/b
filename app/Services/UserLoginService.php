<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 18.09.15
 * Time: 14:08
 */

namespace App\Services;


use App\Contracts\Repositories\UserRepository;
use Auth;
use Illuminate\Auth\Guard;

class UserLoginService
{
    protected $repository;
    /** @var Guard */
    protected $auth;


    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function login($phone, $password)
    {
        return Auth::attempt(['phone' => $phone, 'password' => $password]);
    }
}
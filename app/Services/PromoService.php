<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 16:28
 */

namespace App\Services;

use App\Contracts\Repositories\PromoRepository;
use App\Exceptions\PromoCodeNotFoundException;
use App\References\PromoReference;

class PromoService
{
    /** @var PromoRepository  */
    protected $promoRepository;

    public function __construct(PromoRepository $promoRepository)
    {
        $this->promoRepository = $promoRepository;
    }

    /**
     * @param $promoId
     * @return mixed
     * @throws PromoCodeNotFoundException
     */
    public function apply($promoId)
    {
        if(!($promo = $this->promoRepository->update(['status' => PromoReference::STATUS_USED], $promoId))) {
            throw new PromoCodeNotFoundException;
        }

        return $promo;
    }

}
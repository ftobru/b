<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 20:42
 */

namespace App\Services;


use App\Contracts\Repositories\SalonRepository;
use App\Exceptions\SalonNotFoundException;

class SalonUpdaterService
{
    protected $salonRepository;

    protected $uploaderPicture;


    public function __construct(SalonRepository $salonRepository, UploaderPictureService $uploaderPictureService)
    {
        $this->salonRepository = $salonRepository;
        $this->uploaderPicture = $uploaderPictureService;
    }


    /**
     * @param $id
     * @param array $salonData
     * @param null $pictures
     * @return mixed
     * @throws SalonNotFoundException
     */
    public function update($id, array $salonData, $pictures = null)
    {
        $salon = $this->salonRepository->update($salonData, $id);
        if(!$salon) {
            throw new SalonNotFoundException;
        }
        if($pictures) {
            $this->uploaderPicture; // @todo save picture
        }
        return $salon;
    }
}
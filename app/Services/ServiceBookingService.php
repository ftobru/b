<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 15:15
 */

namespace App\Services;

use App\Contracts\Repositories\BookingRepository;
use App\Contracts\Repositories\PromoRepository;
use App\Contracts\Repositories\ServicesRepository;
use App\Entities\Booking;
use App\Exceptions\ServiceNotFoundException;
use App\ObjectValues\BookingObject;
use Carbon\Carbon;

class ServiceBookingService
{
    /** @var BookingRepository  */
    protected $bookingRepository;
    /** @var ServicesRepository  */
    protected $serviceRepository;


    public function __construct(
        BookingRepository $bookingRepository,
        ServicesRepository $servicesRepository
    )
    {
        $this->bookingRepository = $bookingRepository;
        $this->serviceRepository = $servicesRepository;
    }

    /**
     * @param BookingObject $bookingObject
     * @return Booking
     * @throws ServiceNotFoundException
     */
    public function booking(BookingObject $bookingObject)
    {
        $bookingObject->setEndTime(
            $this->calculateEndTime(
                $bookingObject->getServiceId(),
                $bookingObject->getStartTime()
            )
        );

        return $this->bookingRepository->create($bookingObject->toArray());
    }

    public function calculateEndTime($serviceId, $startTime)
    {
        $service = $this->serviceRepository->find($serviceId);

        if(!$service) {
            throw new ServiceNotFoundException;
        }
        $startTime = new Carbon($startTime);
        return $startTime->addMinute($service->duration);
    }
}
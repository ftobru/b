<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 22.08.15
 * Time: 16:50
 */

namespace App\Providers;


use App\Repositories\Eloquent\AdminRepositoryEloquent;
use App\Repositories\Eloquent\BookingRepositoryEloquent;
use App\Repositories\Eloquent\BrandRepositoryEloquent;
use App\Repositories\Eloquent\CommentRepositoryEloquent;
use App\Repositories\Eloquent\EmailRepositoryEloquent;
use App\Repositories\Eloquent\PaymentsRepositoryEloquent;
use App\Repositories\Eloquent\PhoneRepositoryEloquent;
use App\Repositories\Eloquent\PictureRepositoryEloquent;
use App\Repositories\Eloquent\PromoRepositoryEloquent;
use App\Repositories\Eloquent\ReservationRepositoryEloquent;
use App\Repositories\Eloquent\RoutineRepositoryEloquent;
use App\Repositories\Eloquent\RoutineStaffRepositoryEloquent;
use App\Repositories\Eloquent\SalonRepositoryEloquent;
use App\Repositories\Eloquent\ServicesRepositoryEloquent;
use App\Repositories\Eloquent\SiteRepositoryEloquent;
use App\Repositories\Eloquent\SmsRepositoryEloquent;
use App\Repositories\Eloquent\StaffRepositoryEloquent;
use App\Repositories\Eloquent\UserRepositoryEloquent;
use App\Repositories\Eloquent\VendorRepositoryEloquent;
use App\Repositories\Eloquent\AreaRepositoryEloquent;
use Illuminate\Support\ServiceProvider;


/**
 * Class RepositoryServiceProvider
 * @package App\Providerss
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Repositories\VendorRepository', VendorRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\UserRepository', UserRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\AdminRepository', AdminRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\CommentRepository', CommentRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\PhoneRepository', PhoneRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\SalonRepository', SalonRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\BrandRepository', BrandRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\StaffRepository', StaffRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\ServicesRepository', ServicesRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\BookingRepository', BookingRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\EmailRepository', EmailRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\SiteRepository', SiteRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\PictureRepository', PictureRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\RoutineStaffRepository', RoutineStaffRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\RoutineRepository', RoutineRepositoryEloquent::class
        );


        $this->app->bind(
            'App\Contracts\Repositories\SmsRepository', SmsRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\PaymentsRepository', PaymentsRepositoryEloquent::class
        );

        $this->app->bind(
            'App\Contracts\Repositories\PromoRepository', PromoRepositoryEloquent::class
        );
        
        $this->app->bind(
            'App\Contracts\Repositories\AreaRepository', AreaRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Contracts\Repositories\ReservationRepository', ReservationRepositoryEloquent::class
        );
    }

}
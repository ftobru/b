<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 23.08.15
 * Time: 16:55
 */

namespace App\Providers;

use App\Validators\Rules\CustomRule;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->validator->resolver(function($translator, $data, $rules, $messages){
            return new CustomRule($translator, $data, $rules, $messages);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {}

}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 13:46
 */

namespace App\Providers;

use App\Services\SmsTransportService;
use Clickatell\Api\ClickatellRest;
use Config;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Clickatell\TransportInterface', function($app){
            return new ClickatellRest(Config::get('sms.token'));
        });
        $this->app->bind('App\Contracts\Services\SmsTransportInterface', SmsTransportService::class);
    }
}
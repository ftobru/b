<?php

namespace App\Presenters;

use App\Transformers\SalonTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SalonPresenter
 *
 * @package namespace App\Presenters;
 */
class SalonPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SalonTransformer();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 16:22
 */

namespace App\Exceptions;

use Exception;

/**
 * Class ItemsNotInstanceRecipientObjectException
 * @package App\Exceptions
 */
class ItemsNotInstanceRecipientObjectException extends Exception
{
    //@todo add message
}

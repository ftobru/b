<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 13:16
 */

namespace App\Exceptions;

use Exception;

class UserNotFoundException extends Exception
{
    protected $message = 'User not found';
}
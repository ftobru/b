<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 20:16
 */

namespace App\Exceptions;


use Exception;

/**
 * Class RoutineExistException
 * @package App\Exceptions
 */
class RoutineExistException extends Exception
{
    protected $message = 'Routine exist';
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 16:21
 */

namespace App\Exceptions;

use Exception;

/**
 * Class ServiceNotFoundException
 * @package App\Exceptions
 */
class ServiceNotFoundException extends Exception
{
    protected $message = 'Service not found';

    protected $code = 404;
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 18:14
 */

namespace App\Exceptions;


use Exception;

/**
 * Class VendorNotFoundException
 * @package App\Exceptions
 */
class VendorNotFoundException extends Exception
{
    protected $code = 404;

    protected $message = 'Vendor not found';
}
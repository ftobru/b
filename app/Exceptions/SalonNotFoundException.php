<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 18:12
 */

namespace App\Exceptions;

use Exception;

/**
 * Class SalonNotFoundException
 * @package App\Exceptions
 */
class SalonNotFoundException extends Exception
{
    protected $message = 'Salon not found';

    protected $code = 404;
}
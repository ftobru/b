<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 31.10.15
 * Time: 15:08
 */

namespace App\Exceptions;

use Exception;

class PromoCodeNotFoundException extends Exception
{
    protected $message = 'Promo code not found';

    protected $code = 404;
}
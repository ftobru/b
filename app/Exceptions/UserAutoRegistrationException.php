<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 21:36
 */

namespace App\Exceptions;


use Exception;

class UserAutoRegistrationException extends Exception
{
    // message
    protected $message = 'User auto registration fail';
}
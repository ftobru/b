<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 21.09.15
 * Time: 15:17
 */
namespace App\Exceptions;

use Exception;

class TypeNotFoundException extends Exception
{
    //
}
<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Vendor;

/**
 * Class VendorTransformer
 * @package namespace App\Transformers;
 */
class VendorTransformer extends TransformerAbstract
{

    /**
     * Transform the \Vendor entity
     * @param \Vendor $model
     *
     * @return array
     */
    public function transform(Vendor $model) {
        return [
            'id'         => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Salon;

/**
 * Class SalonTransformer
 * @package namespace App\Transformers;
 */
class SalonTransformer extends TransformerAbstract
{

    /**
     * Transform the \Salon entity
     * @param \Salon $model
     *
     * @return array
     */
    public function transform(Salon $model) {
        return [
            'id'         => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
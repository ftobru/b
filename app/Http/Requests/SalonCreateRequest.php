<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 19:50
 */

namespace App\Http\Requests;

class SalonCreateRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required',
            'name' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'type' > 'required'
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 18:53
 */

namespace App\Exceptions;

use Exception;

/**
 * Class NotCreateVendorException
 * @package App\Exceptions
 */
class NotCreateVendorException extends Exception
{
    /** @var string  */
    protected $message = 'Not create vendor';
}
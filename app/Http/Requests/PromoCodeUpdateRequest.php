<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 17.10.15
 * Time: 13:28
 */

namespace App\Http\Requests;

class PromoCodeUpdateRequest extends Request
{
    public function rules()
    {
        return [
            'code' => 'required',
            'valid_to' => 'date',
            'conditions' => 'array',
        ];
    }
}
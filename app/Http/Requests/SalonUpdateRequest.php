<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 19:29
 */
namespace App\Http\Requests;

/**
 * Class SalonUpdateRequest
 * @package App\Http\Requests
 */
class SalonUpdateRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required',
            'name' => 'required',
            'address' => 'required|min:6|max:255',
            'lat' => 'required',
            'lng' => 'required',
            'rating' => 'required',
            'type' => 'required',
            'postcode' => 'min:3|max:255'
        ];
    }

}
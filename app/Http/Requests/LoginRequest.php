<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 10.10.15
 * Time: 15:12
 */

namespace App\Http\Requests;

class LoginRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|min:6|max:255',
            'password' => 'required|min:6|max:255',
        ];
    }
}
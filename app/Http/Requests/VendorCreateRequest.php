<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 18:21
 */

namespace App\Http\Requests;

class VendorCreateRequest extends Request
{
    public function rules()
    {
        return [
            'vendor.name' => 'required|min:6|max:255',
            'vendor.password' => 'required|min:6|max:255',
            'vendor.password_confirmed' => 'required|min:6|max:255',
            'vendor.phone' => 'required|min:6|max:255',
            'vendor.status' => 'required',
            'vendor.type' => 'required',
            'vendor.email' => 'required|email',
            'salon.status' => 'required',
            'salon.name' => 'required|min:6|max:255',
            'salon.address' => 'required|min:6|max:255',
            'salon.lat' => 'required',
            'salon.lng' => 'required',
            'salon.type' => 'required',
            'salon.postcode' => 'min:3|max:255'
        ];
    }

    public function authorize()
    {
        return false;
    }
}
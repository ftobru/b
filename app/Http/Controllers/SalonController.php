<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 10.10.15
 * Time: 15:54
 */

namespace App\Http\Controllers;

use App\Contracts\Repositories\SalonRepository;
use App\Http\Requests\SalonCreateRequest;
use App\Http\Requests\SalonUpdateRequest;

class SalonController extends DashboardController
{
    protected $salonRepository;

    public function __construct(SalonRepository $repository)
    {
        $this->salonRepository = $repository;
    }

    public function getIndex()
    {
        return view('dashboard.salons.index', ['salons' => $this->salonRepository->paginate()]);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getShow($id)
    {
        if(!($salon = $this->salonRepository->find($id))) {
            abort(404);
        }
        return view('dashboard.salons.show', ['salons' => $salon]);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        if(!($salon = $this->salonRepository->find($id))) {
            abort(404);
        }
        return view('dashboard.salons.edit', ['salons' => $salon]);
    }

    public function postEdit(SalonUpdateRequest $request, $id)
    {

    }

    public function getCreate()
    {
        return view('dashboard.salons.create');
    }

    public function postCreate(SalonCreateRequest $request)
    {

    }
}
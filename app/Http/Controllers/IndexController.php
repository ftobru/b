<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 24.10.15
 * Time: 16:27
 */

namespace App\Http;

use App\Contracts\Repositories\BookingRepository;
use App\Contracts\Repositories\VendorRepository;
use App\Http\Controllers\DashboardController;
use App\References\BookingReference;
use App\References\VendorReference;

class IndexController extends DashboardController
{
    protected $vendorRepository;

    protected $bookingRepository;

    /**
     * @param VendorRepository $repository
     * @param BookingRepository $bookingRepository
     */
    public function __construct(VendorRepository $repository, BookingRepository $bookingRepository)
    {
        $this->vendorRepository = $repository;
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $newVendors = $this->vendorRepository->findByField('status', VendorReference::STATUS_NOT_ACTIVE);
        $newBooking = $this->bookingRepository->findByField('status', BookingReference::STATUS_NEW);
        return view('dashboard.index', [
            'newVendors' => $newVendors,
            'newBooking' => $newBooking
        ]);
    }
}
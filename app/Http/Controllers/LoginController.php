<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 10.10.15
 * Time: 15:11
 */
namespace App\Http\Controllers;


use App\Http\Requests\LoginRequest;
use App\Services\AdminLoginService;

/**
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{
    protected $service;

    /**
     * @param AdminLoginService $service
     */
    public function __construct(AdminLoginService $service)
    {
        $this->service = $service;
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return view('default.login');
    }

    /**
     *
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(LoginRequest $request)
    {
        if($this->service->login(
            $request->get('email'), $request->get('password'), boolval($request->get('rememberMe', false)))
        ) {
            return redirect('/backend/dashboard/vendor');
        }
        return redirect('/backend/login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        return redirect('/backend/login');
    }
}
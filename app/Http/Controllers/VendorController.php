<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 10.10.15
 * Time: 15:31
 */

namespace App\Http\Controllers;


use App\Contracts\Repositories\VendorRepository;
use App\Http\Requests\VendorCreateRequest;
use App\Http\Requests\VendorUpdateRequest;
use App\Services\VendorCreaterService;
use App\Services\VendorUpdaterService;
use Illuminate\Http\Request;

class VendorController extends DashboardController
{
    /**
     * @var VendorRepository
     */
    protected $repository;
    /**
     * @var VendorUpdaterService
     */
    protected $updater;
    /**
     * @var VendorCreaterService
     */
    protected $creater;

    /**
     * @param VendorRepository $repository
     * @param VendorUpdaterService $updater
     * @param VendorCreaterService $creater
     */
    public function __construct(
        VendorRepository $repository,
        VendorUpdaterService $updater,
        VendorCreaterService $creater
    ) {
        $this->repository = $repository;
        $this->updater = $updater;
        $this->creater = $creater;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('dashboard.vendor.index', ['vendors' => $this->repository->paginate()]);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getShow($id)
    {
        if (!($vendor = $this->repository->find($id))) {
            abort(404);
        }

        return view('dashboard.vendor.show', $vendor);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        if (!($vendor = $this->repository->find($id))) {
            abort(404);
        }

        return view('dashboard.vendor.edit');
    }

    /**
     * @param VendorUpdateRequest $request
     * @param $id
     * @throws \App\Exceptions\SalonNotFoundException
     * @throws \App\Exceptions\VendorNotFoundException
     */
    public function postEdit(VendorUpdateRequest $request, $id)
    {
        $this->updater->update($id, $request->get('vendor'), $request->get('salon', []));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        return view('dashboard.vendor.create');
    }

    /**
     * @param VendorCreateRequest $request
     * @return \App\Entities\Vendor
     * @throws \App\Exceptions\NotCreateVendorException
     */
    public function postCreate(Request $request)
    {

        $this->creater->createWithPicture($request->get('vendor'), $request->get('salon', []), $request->files->all());
    }
}
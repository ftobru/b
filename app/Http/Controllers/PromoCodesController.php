<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 16.10.15
 * Time: 20:56
 */


namespace App\Http\Controllers;

use App\Contracts\Repositories\PromoRepository;
use App\Http\Requests\PromoCodeUpdateRequest;

/**
 * Class PromoCodesController
 * @package App\Http\Controllers
 */
class PromoCodesController extends DashboardController
{
    /** @var PromoRepository  */
    protected $promoCodesRepository;

    /**
     * @param PromoRepository $repository
     */
    public function __construct(PromoRepository $repository)
    {
        $this->promoCodesRepository = $repository;
    }

    public function getIndex()
    {
        return view('dashboard.promo.index', ['promoCodes' => $this->promoCodesRepository->with('salons')->paginate()]);
    }

    public function getShow($id)
    {
        if(!($promo = $this->promoCodesRepository->find($id))) {
            abort(404);
        }
        return view('dashboard.promo.show', ['promoCodes' => $promo]);
    }

    public function getEdit($id)
    {
        if(!($promo = $this->promoCodesRepository->find($id))) {
            abort(404);
        }
        return view('dashboard.promo.edit', ['promoCodes' => $promo]);
    }

    public function postEdit(PromoCodeUpdateRequest $request, $id)
    {
        return $this->promoCodesRepository->update($request->all(), $id);
    }

    public function getCreate()
    {
        return view('dashboard.promo.create');
    }

    public function postCreate()
    {

    }
}
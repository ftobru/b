<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 10.10.15
 * Time: 15:37
 */

namespace App\Http\Controllers;

use App\Contracts\Repositories\BookingRepository;

class BookingController extends DashboardController
{
    protected $repository;

    /**
     * @param BookingRepository $repository
     */
    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getIndex()
    {
        return view('dashboard.booking.index');
    }
}
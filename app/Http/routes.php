<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    return view('layouts/docs')->with('content', \Markdown::convertToHtml(view('docs/documentation')->__toString()));
});


Route::get('backend/login', ['uses' => 'LoginController@getLogin']);
Route::post('backend/login', ['uses' => 'LoginController@postLogin']);
Route::get('backend/logout', ['uses' => 'LoginController@getLogout', 'middleware' => 'auth.admin']);

Route::group(['prefix' => 'backend/dashboard', 'middleware' => ['auth.admin']], function(){

    Route::get('/', ['uses' => 'IndexController@getIndex', 'as' => 'dashboard.index']);

    Route::group(['prefix' => 'vendor'], function(){
        Route::get('/', ['uses' => 'VendorController@getIndex', 'as' => 'dashboard.vendor.index']);
        Route::get('/create', ['uses' => 'VendorController@getCreate', 'as' => 'dashboard.vendor.create']);
        Route::post('/create', ['uses' => 'VendorController@postCreate', 'as' => 'dashboard.vendor.create']);
        Route::get('/edit/:id', ['uses' => 'VendorController@getEdit', 'as' => 'dashboard.vendor.edits']);
    });

    Route::group(['prefix' => 'booking'], function(){
        Route::get('/', ['uses' => 'BookingController@getIndex', 'as' => 'dashboard.booking.index']);
    });
});

<?php
/**
 * Created by PhpStorm.
 * User: volkov
 * Date: 30.10.15
 * Time: 14:02
 */

namespace App\Contracts\Services;

use App\Collections\RecipientsCollection;

interface SmsTransportInterface
{
    /**
     * @param RecipientsCollection $recipientsCollections
     * @param $message
     * @param array $extra
     * @return mixed
     * @internal param array $extra
     */
    public function sendMessage(RecipientsCollection $recipientsCollections, $message, $extra = []);

    /**
     * Get Balance (By Gate)
     * @return mixed
     */
    public function getBalance();

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function stopMessage($apiMsgId);

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function queryMessage($apiMsgId);

    /**
     * @param $msisdn
     * @return mixed
     */
    public function routeCoverage($msisdn);

    /**
     * @param $apiMsgId
     * @return mixed
     */
    public function getMessageCharge($apiMsgId);
}
<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SiteRepository
 * @package namespace App\Contracts\Repositories;
 */
interface SiteRepository extends RepositoryInterface
{
    //
}

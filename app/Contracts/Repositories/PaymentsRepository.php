<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentsRepository
 * @package namespace App\Contracts\Repositories;
 */
interface PaymentsRepository extends RepositoryInterface
{
    //
}

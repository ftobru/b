<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServivesRepository
 * @package namespace App\Contracts\Repositories;
 */
interface ServicesRepository extends RepositoryInterface
{
    //
}

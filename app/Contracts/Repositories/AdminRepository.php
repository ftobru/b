<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdminRepository
 * @package namespace App\Repositories;
 */
interface AdminRepository extends RepositoryInterface
{
    //
}

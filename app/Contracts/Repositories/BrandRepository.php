<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BrandRepository
 * @package namespace App\Repositories;
 */
interface BrandRepository extends RepositoryInterface
{
    //
}

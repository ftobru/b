<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentRepository
 * @package namespace App\Repositories;
 */
interface CommentRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhoneRepository
 * @package namespace App\Repositories;
 */
interface PhoneRepository extends RepositoryInterface
{
    //
}

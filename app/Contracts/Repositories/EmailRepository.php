<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmailRepository
 * @package namespace App\Contracts\Repositories;
 */
interface EmailRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoutineStaffRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RoutineStaffRepository extends RepositoryInterface
{
    //
}

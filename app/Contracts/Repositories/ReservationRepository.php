<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReservationRepository
 * @package namespace App\Contracts\Repositories;
 */
interface ReservationRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SmsRepository
 * @package namespace App\Contracts\Repositories;
 */
interface SmsRepository extends RepositoryInterface
{
    //
}

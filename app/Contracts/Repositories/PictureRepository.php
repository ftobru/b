<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PictureRepository
 * @package namespace App\Contracts\Repositories;
 */
interface PictureRepository extends RepositoryInterface
{
    //
}

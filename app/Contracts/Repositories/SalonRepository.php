<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SalonRepository
 * @package namespace App\Repositories;
 */
interface SalonRepository extends RepositoryInterface
{
    //
}

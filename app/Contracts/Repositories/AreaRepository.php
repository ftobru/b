<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaRepository
 * @package namespace App\Contracts\Repositories;
 */
interface AreaRepository extends RepositoryInterface
{
    //
}

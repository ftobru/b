<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VendorRepository
 * @package namespace App\Repositories;
 */
interface VendorRepository extends RepositoryInterface
{
    //
}

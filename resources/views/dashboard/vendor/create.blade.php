@extends('layouts.default')
@section('content')
    <div class="card-head">
        <header>
            <h2 class="opacity-50">Registration customer</h2>
        </header>
    </div>
    <div class="card card-outlined style-default-light">
        <div class="card-body style-default-dark">
            <form class="form form-inverse" enctype="multipart/form-data"  action="" method="post">
                <div class="form-group">
                    <input type="text" name="vendor[name]" id="name" autocomplete="off" class="form-control">
                    <label for="name">Name</label>
                </div>
                <div class="form-group">
                    <input type="email" name="vendor[email]" autocomplete="off" id="email2" class="form-control">
                    <label for="email2">Email</label>
                </div>
                <div class="form-group">
                    <input type="password" name="vendor[password]" id="password2" autocomplete="off" class="form-control">
                    <label for="password">Password</label>
                </div>
                <div class="form-group">
                    <input type="text" name="vendor[phone]" id="phone2" autocomplete="off" class="form-control">
                    <label for="phone2">Phone</label>
                </div>
                <div class="form-group">
                    <input type="text" name="salon[name]" id="salon_name" class="form-control">
                    <label for="salon_name">Salon name</label>
                </div>
                <div class="form-group">
                    <input type="text" name="salon[address]" id="salon_address" class="form-control">
                    <label for="salon_address">Address</label>
                </div>
                <div class="form-group">
                    <input type="text" name="salon[postcode]" id="salon_postcode" class="form-control">
                    <label for="salon_postcode">Postcode</label>
                </div>
                <div class="form-group">
                    <select name="serviceType" id="serviceType" class="form-control">
                        @foreach(app(\App\References\ServiceTypeReference::class)->types() as $id => $type):
                            <option value="{{$id}}">{{$type}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  {"255х155": "/home/volkov/Workspace/beautyme/public/images/salons/a9b232dedf79bde57fb30c988dd5f0e2.jpg", "580х340": "/home/volkov/Workspace/beautyme/public/images/salons/27388b2a066d35d152ef172835ec6e8b.jpg"} -->
                <div class="form-group">
                    <label for="salon_picture_580_340">Picture</label>
                    <input name="salon_picture_580х340" type="file" id="salon_picture_580_340">
                    <p class="help-block">Picture 580х340</p>
                </div>
                <div class="form-group">
                    <label for="salon_picture_255_155">Picture</label>
                    <input type="file" name="salon_picture_255_155" id="salon_picture_255_155">
                    <p class="help-block">Picture 255х155</p>
                </div>
                <div class="form-group">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" id="cb5"><span></span> Activation
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Registration</button>
                    <button type="button" class="btn btn-default">Reset</button>
                </div>
            </form>
        </div>
    </div>
@endsection
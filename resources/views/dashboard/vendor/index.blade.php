@extends('layouts.default')
    @section('content')
        <div class="section-header">
        <ol class="breadcrumb">
        <li class="active">Customers</li>
        </ol>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8 col-sm-4"></div>
            <div class="col-md-4 col-lg-4 col-sm-4">
                <a href="{{route('dashboard.vendor.create')}}" class="btn btn-primary">REGISTRATION CUSTOMER</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th>ID #</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Date registration</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                @foreach($vendors as $vendor)
                    <tr class="@if($vendor->status === 0):
                     danger
                     @elseif($vendor->status === 1)
                            success
                     @endif">
                        <td>{{$vendor->id}}</td>
                        <td>
                            {{$vendor->name}}
                        </td>
                        <td>{{$vendor->email}}</td>
                        <td>{{$vendor->phone}}</td>
                        <td>{{$vendor->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" class="btn btn-xs btn-default">Edit</button>
                            <button type="button" class="btn btn-xs btn-success">Active</button>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        {!! $vendors->render() !!}
    @endsection
@extends('layouts.welcome')
        @section('content')
        <!-- BEGIN LOGIN SECTION -->
        <section class="section-account">
            <div class="spacer"></div>
            <div class="card contain-sm style-transparent">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2"></div>
                        <div class="col-sm-6 col-md-8 col-lg-8">
                            <div class="card card-bordered style-primary">
                                <div class="card-head">
                                    <header>BFAB - BACKEND</header>
                                </div>

                                <div class="card-body style-default-bright">
                                    <form class="form floating-label" action="" accept-charset="utf-8" method="post">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email" name="email">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password" name="password">
                                            <label for="password">Password</label>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-xs-6 text-left">
                                                <div class="checkbox checkbox-inline checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" name="rememberMe"> <span>Remember me</span>
                                                    </label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-xs-6 text-right">
                                                <button class="btn btn-primary btn-raised" type="submit">Login</button>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                    </form>
                                </div>
                            </div>
                        </div><!--end .col -->
                        <div class="col-sm-5 col-sm-offset-1 text-center">
                        </div><!--end .col -->
                    </div><!--end .row -->
                </div><!--end .card-body -->
            </div><!--end .card -->
        </section>
        <!-- END LOGIN SECTION -->
        @endsection

<?php



class PaymentCept extends ApiTestCase
{
    public function generateToken()
    {
        $this->I->wantTo('Generate token');
        $this->I->sendGET('/payment/token');
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);

    }

    public function pay()
    {
        $this->I->wantTo('Pay');
        $this->I->sendPOST('/payment/pay', [
            'amount' => 1000,
            'paymentMethodNonce' =>'pay'
        ]);
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
    }
}


$test = new PaymentCept(new ApiTester($scenario));
$test->generateToken();
$test->pay();

<?php


class VendorLoginCept extends ApiTestCase
{
    public function login()
    {
        $this->I->am('Vendor login test');
        $this->I->wantTo('Create Vendor');
        $vendor = $this->haveVendor();
        $loginData = [
            'phone' => '+791345500432',
            'password' => 'test'
        ];
        $this->I->sendPOST('/login', $loginData);
        $this->I->seeResponseCodeIs(200);
    }

}

$test = new VendorLoginCept(new ApiTester($scenario));
$test->login();

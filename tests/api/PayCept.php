<?php



class PayCept extends ApiTestCase
{

    public function store()
    {
        $this->I->sendPOST('/payment/pay', []);
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
    }
}

$test = new PayCept(new ApiTester($scenario));
$test->store();
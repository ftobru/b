<?php 


class SalonTypesCept extends ApiTestCase
{
    public function getAll()
    {
        $this->I->am("User");
        $this->I->wantTo("Get all types");
        $this->I->sendGET("/salonsTypes");
        $this->I->seeResponseCodeIs(200);
    }
}

$salonsTypes = new SalonTypesCept(new ApiTester($scenario));
$salonsTypes->getAll();
<?php
// Here you can initialize variables that will be available to your tests


use Helper\FactoryHelper;

abstract class ApiTestCase
{
    /** @var  ApiTester */
    protected $I;
    /** @var  Faker\Generator */
    protected $faker;

    protected $helper;

    /**
     * @param ApiTester $I
     */
    public function __construct(ApiTester $I)
    {
        $this->faker = Faker\Factory::create();
        $this->I = $I;
        $this->I->haveHttpHeader('Accept', 'application/vnd.BeautyMe.v1+json');

    }


    /**
     * @return mixed
     */
    protected function grabResponseJson()
    {
        return json_decode($this->I->grabResponse());
    }


    protected function haveVendor($phone = '+79134550042', $password = 'test')
    {
        return factory(\App\Entities\Vendor::class)
            ->create(['password' => $password, 'phone' => $phone])
            ->salons()->save(factory(\App\Entities\Salon::class)->make());
    }


}
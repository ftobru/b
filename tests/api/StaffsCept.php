<?php 

class StaffsCept extends ApiTestCase
{

    protected $staff;

    public function get()
    {
        $this->I->am('staffs test');
        $this->I->wantToTest('Get staffs');
        $this->I->sendGET('/staffs');
        $this->I->seeResponseCodeIs(200);
        $this->I->grabResponse();

    }

    public function store()
    {
        $this->I->wantToTest('Create staff');
        $phone = $this->faker->phoneNumber;
        $password = $this->faker->password();
        $this->haveVendor($phone, $password);

        $this->I->sendPOST('/vendor/login', [
            'phone' => $phone,
            'password' => $password
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->sendPOST('/staffs', [
            'name' => $this->faker->name,
            'title' => $this->faker->title
        ]);
        $this->I->seeResponseCodeIs(200);

    }


    public function delete()
    {
        $this->I->wantToTest('Create staff');
        $phone = $this->faker->phoneNumber;
        $password = $this->faker->password();
        $this->haveVendor($phone, $password);

        $this->I->sendPOST('/vendor/login', [
            'phone' => $phone,
            'password' => $password
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->sendDELETE('/staffs/' . 1);
        $this->I->seeResponseCodeIs(200);
    }



}

$test = new StaffsCept(new ApiTester($scenario));
$test->get();
$test->store();
//$test->delete();

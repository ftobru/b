<?php


/**
 * Class ServiceTypesCept
 */
class ServiceTypesCept extends ApiTestCase
{
    public function getTypes()
    {
        $this->I->wantTo('Get types services');

        $this->I->sendGET('/serviceTypes');
        $this->I->seeResponseCodeIs(200);
    }
}

$test = new ServiceTypesCept(new ApiTester($scenario));
$test->getTypes();

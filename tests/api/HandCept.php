<?php

class HandCept extends ApiTestCase
{

    public function check()
    {
        $this->I->wantTo("fetch token and use it as bearer");
        $this->I->sendGET('/hand');
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();

    }
}


$handCept = new HandCept(new ApiTester($scenario));
$handCept->check();
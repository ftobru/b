<?php 

class GetVendorsCept extends ApiTestCase
{

    public function get()
    {
        $this->I->am("Get Vedor test");
        $this->I->sendGET('/vendors');
        $this->I->seeResponseCodeIs(200);
        $this->grabResponseJson();
    }
}



/** @var GetVendorsCept $vendorsCept */
$vendorsCept = (new GetVendorsCept(new ApiTester($scenario)));
$vendorsCept->get();
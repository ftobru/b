<?php
use Illuminate\Support\Collection;

/**
 * Registration vendor Test
 * Class VendorRegistrationCept
 */

class VendorRegistrationCept extends ApiTestCase
{

    /**
     *
     * salon: {name: "Test Biz", address: "Перлис MY ", lat: 6.444912899999999, lng: 100.20476910000002,…}
     * address: "Перлис MY "
     * lat: 6.444912899999999
     * lng: 100.20476910000002
     * name: "Test Biz"
     * phones: "12345678900"
     * postcode: "12345"
    type: 43
    vendor: {name: "Test Name", email: "test@email.com", phone: "12345678900"}
    email: "test@email.com"
    name: "Test Name"
    phone: "12345678900"
     *
     *
     *
     *
     */
    public function registration()
    {
//        $vendorData = [
//            'vendor' => [
//                'name' => "Test Name",
//                'email' => "test@email.com",
//                'phone' => "12345678900"
//            ],
//            'salon' => [
//                'address' => "Перлис MY ",
//                'lat' => 6.444912899999999,
//                'lng' => 100.20476910000002,
//                'postcode' => "12345",
//                'type' => 43,
//                "phones" => "12345678900"
//
//            ]
//
//        ];
        $vendorData = [
            'vendor' => [
                'name' => $this->faker->name,
                'email' => $this->faker->email,
                'phone' => $this->faker->phoneNumber,
            ],
            'salon' => [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'lat' => $this->faker->latitude,
                'lng' => $this->faker->longitude,
                'postcode' => $this->faker->postcode,
                'type' => (new Collection(\App\References\SalonReference::typesId()))->random(),
                'i_agree' => true

            ]

        ];
        $this->I->wantTo('Test registration vendor');
        $this->I->sendPost('/vendor/registration', $vendorData);
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }
}

$registrationCept = new VendorRegistrationCept(new ApiTester($scenario));
$registrationCept->registration();
<?php


class FilterSalonsCept extends ApiTestCase
{
    public function getSalons()
    {
        $this->I->wantTo('Filter salons');

        $this->I->sendGET('/salonsFilter', [
            'sortPriceLower' => 'Y',
            'sortPriceHigher' => 'Y',
            'sortRating' => 'Y'
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->grabResponse();
    }
}

$test = new FilterSalonsCept(new ApiTester($scenario));
$test->getSalons();
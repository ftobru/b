<?php 


class SalonCept extends ApiTestCase
{
    public function getAll()
    {
        $this->I->wantTo('Get salons');
        $this->I->sendGET('/salons', ['limit' => 10]);
        $this->I->seeResponseCodeIs(200);


    }

    public function getSalonsWithPictures()
    {
        $this->I->wantTo('Get top salons');
        $this->I->sendGET('/salons', [
            'with' => 'pictures'
        ]);
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSalonsWithRoutines()
    {
        $this->I->wantTo('Get salons with Routines');
        $this->I->sendGET('/salons', [
            'with' => 'routines'
        ]);

        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSalonsWithSettings()
    {
        $this->I->wantTo('Get salons with settings');

        $this->I->sendGET('/salons', [
            'with' => 'settings'
        ]);

        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
    }
}

$salonCept = new SalonCept(new ApiTester($scenario));
$salonCept->getAll();
$salonCept->getSalonsWithPictures();
$salonCept->getSalonsWithRoutines();
$salonCept->getSalonsWithSettings();
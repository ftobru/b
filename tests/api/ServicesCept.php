<?php 


class ServiceCept extends ApiTestCase
{
    public function get()
    {
        $this->I->am('service test');
        $this->I->wantToTest('Get services');
        $this->I->sendGET('/services');
        $this->I->seeResponseCodeIs(200);
    }

    public function store()
    {
        $this->I->wantToTest('Create service');

        $phone = $this->faker->phoneNumber;
        $password = $this->faker->password();
        $this->haveVendor($phone, $password);

        $this->I->sendPOST('/vendor/login', [
            'phone' => $phone,
            'password' => $password
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->sendPOST('/services', [
            'name' => $this->faker->name,
            'duration' => $this->faker->numberBetween(1, 1000),
            'price' => $this->faker->numberBetween(1, 10000)
        ]);

        $this->I->seeResponseCodeIs(200);
    }


}

$test = new ServiceCept(new ApiTester($scenario));
$test->get();
$test->store();
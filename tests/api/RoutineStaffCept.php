<?php


class RoutineStaffTest extends ApiTestCase
{
    public function getRoutineStaff()
    {
        $this->I->wantTo('Routine Staff');
        $this->I->sendGET('/routineStaffs');
        $this->I->seeResponseCodeIs(200);
        $this->I->grabResponse();
    }
}


$test = new RoutineStaffTest(new ApiTester($scenario));
$test->getRoutineStaff();
<?php


use App\Collections\RecipientsCollection;
use App\ObjectValues\RecipientObject;
use App\References\SmsReference;
use App\Services\SmsTransportService;

class SmsTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /** @var  SmsTransportService */
    protected $service;

    protected function _before()
    {
        /** @var SmsTransportService $smsService */
        $this->service = App::make('App\Contracts\Services\SmsTransportInterface');
    }

    protected function _after()
    {
    }

    // tests
    public function testInit()
    {
        $this->assertInstanceOf(SmsTransportService::class, $this->service);
    }

    public function testBalance()
    {
        $this->assertObjectHasAttribute('balance', $this->service->getBalance(), 'Не возможно получить баланс');
    }

    public function testSend()
    {
        $collection = new RecipientsCollection([new RecipientObject(1, '+79134550042', SmsReference::TYPE_ADMIN)]);
        $this->service->sendMessage($collection, 'Test');
    }


}
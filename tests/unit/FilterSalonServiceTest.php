<?php


class FilterSalonServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var \App\Services\FilterSalonService
     */
    protected $service;

    protected function _before()
    {
        $this->service = App::make(\App\Services\FilterSalonService::class);
    }

    protected function _after()
    {

    }

    // tests
    public function testAll()
    {
        $this->assertInstanceOf(\App\Services\FilterSalonService::class, $this->service);
        $salons = $this->service
            ->setBrands([1])
            ->setPriceRange(1, 1000000)
            ->setAvailibility('12:12:2015', '00:00:01', '23:59:59')
            ->setServices([1])
            ->setServiceTypes([1])
            ->all();
        dd($salons);
        $this->assertTrue(is_array($salons));
    }

}
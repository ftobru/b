<?php


use App\Services\VendorCreaterService;

class CreateVendorWithPictureServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreate()
    {
        $service = App::make(VendorCreaterService::class);
        $faker  = Faker\Factory::create();
        $vendorData = [
            'name' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'password' => $faker->password,
        ];

        $salonData = [
            'name' => $faker->name,
            'address' => $faker->address,
            'postcode' => $faker->postcode
        ];
        $image = $faker->image('/tmp');


        $pictures = [
            'salon_picture_580_340' => new \Symfony\Component\HttpFoundation\File\UploadedFile($image, 'test.jpg')
        ];

        $vendor = $service->createWithPicture($vendorData, $salonData, $pictures);
        $this->assertInstanceOf(\App\Entities\Vendor::class, $vendor);
    }
}
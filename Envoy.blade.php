@servers(['web' => 'bfab.ru'])

@task('backend:migrate')
    cd /usr/share/nginx/backend
    php artisan migrate --force
@endtask

@task('backend:seed')
    cd /usr/share/nginx/backend
    php artisan db:seed --force

@endtask
@task('backend:down')
    cd /usr/share/nginx/backend
    php artisan down
    {{--sudo supervisorctl stop saas-socket--}}
@endtask

@task('backend:update')
    cd /usr/share/nginx/backend

    git fetch --all
    git reset --hard origin/master
    git pull

    composer update

    chmod -R 777 /usr/share/nginx/backend/storage
    chmod +x /usr/share/nginx/backend/artisan
@endtask

@task('backend:up')
    cd /usr/share/nginx/backend
    php artisan up

    {{--sudo supervisorctl start saas-socket--}}
@endtask

@macro('backend:deploy')
    backend:down
    backend:update
    backend:migrate
    backend:up
@endmacro
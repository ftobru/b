<?php

return [

    'multi' => [
        'admin' => [
            'driver' => 'eloquent',
            'model' => \App\Entities\Admin::class,
        ],
        'client' => [
            'driver' => 'eloquent',
            'model' => \App\Entities\User::class,
        ],
        'vendor' => [
            'driver' => 'eloquent',
            'model' => \App\Entities\Vendor::class
        ]
    ],

    'password' => [
        'email' => 'emails.password',
        'table' => 'password_resets',
        'expire' => 60,
    ],

];